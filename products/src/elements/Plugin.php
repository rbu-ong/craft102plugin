<?php
namespace craft\products\elements;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQueryInterface;
use craft\products\elements\db\ProductQuery;
use craft\elements\actions\Delete;

class Plugin extends Element
{
    /**
     * @var int Price
     */
    public $price = 0;

    /**
     * @var string Currency code
     */
    public $currency;

    // ...

    public static function hasTitles(): bool
	{
	    return true;
	}

	public static function hasContent(): bool
	{
	    return true;
	}

	// public function getEditorHtml(): string
	// {
	//     $html = \Craft::$app->getView()->renderTemplateMacro('_includes/forms', 'textField', [
	//         [
	//             'label' => \Craft::t('app', 'Title'),
	//             'siteId' => $this->siteId,
	//             'id' => 'title',
	//             'name' => 'title',
	//             'value' => $this->title,
	//             'errors' => $this->getErrors('title'),
	//             'first' => true,
	//             'autofocus' => true,
	//             'required' => true
	//         ]
	//     ]);

	//     // ...

	//     $html .= parent::getEditorHtml();

	//     return $html;
	// }

	// public function getFieldLayout()
	// {
	//     return \Craft::$app->fields->getLayoutByType(Product::class);
	// }

	public static function isLocalized(): bool
	{
	    return true;
	}

	/**
     * @inheritdoc
     */
    protected static function defineActions(string $source = null): array
    {
        // Now figure out what we can do with these
        $actions = [];

        $actions[] = Craft::$app->getElements()->createAction([
            'type' => Delete::class,
            'confirmationMessage' => Craft::t('app', 'Are you sure you want to delete the selected entries?'),
            'successMessage' => Craft::t('app', 'Products deleted.'),
        ]);

        return $actions;
    }

    public function afterSave(bool $isNew)
	{
	    if ($isNew) {
	        \Craft::$app->db->createCommand()
	            ->insert('{{%products}}', [
	                'id' => $this->id,
	                'price' => $this->price,
	                'currency' => $this->currency,
	            ])
	            ->execute();
	    } else {
	        \Craft::$app->db->createCommand()
	            ->update('{{%products}}', [
	                'price' => $this->price,
	                'currency' => $this->currency,
	            ], ['id' => $this->id])
	            ->execute();
	    }

	    parent::afterSave($isNew);
	}

	public static function find(): ElementQueryInterface
    {
        return new ProductQuery(static::class);
    }

    protected static function defineSources(string $context = null): array
	{
	    return [
	        [
	            'key' => '*',
	            'label' => 'All Products',
	            'criteria' => []
	        ],
	        [
	            'key' => 'cad',
	            'label' => 'CAD',
	            'criteria' => [
	                'currency' => 'cad',
	            ]
	        ],
	        [
	            'key' => 'usd',
	            'label' => 'USD',
	            'criteria' => [
	                'currency' => 'usd',
	            ]
	        ],
	    ];
	}

	// public function getCpEditUrl()
	// {
	//     return 'products/products/'.$this->id;
	// }

	// protected static function defineActions(string $source = null): array
	// {
	//     return [
	//         FooAction::class,
	//         BarAction::class,
	//     ];
	// }

	protected static function defineSortOptions(): array
	{
	    return [
	        'title' => \Craft::t('app', 'Title'),
	        'price' => \Craft::t('products', 'Price'),
	    ];
	}

	protected static function defineTableAttributes(): array
	{
	    return [
	        'title' => \Craft::t('app', 'Title'),
	        'price' => \Craft::t('products', 'Price'),
	        'currency' => \Craft::t('products', 'Currency'),
	    ];
	}

	protected static function defineDefaultTableAttributes(string $source): array
	{
	    return ['title', 'price', 'currency'];
	}

	protected function tableAttributeHtml(string $attribute): string
	{
	    switch ($attribute) {
	        case 'price':
	            return \Craft::$app->formatter->asCurrency($this->price);

	        case 'currency':
	            return \Craft::$app->formatter->asCurrency($this->currency);
	    }

	    return parent::tableAttributeHtml($attribute);
	}

}