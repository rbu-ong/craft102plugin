<?php

namespace craft\products\migrations;

use Craft;
use craft\db\Migration;

/**
 * m180109_044245_pluginmigrationfile migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        if (!$this->db->tableExists('{{%products}}')) {
            // create the products table
            $this->createTable('{{%products}}', [
                'id' => $this->integer()->notNull(),
                'price' => $this->integer()->notNull(),
                'currency' => $this->char(3)->notNull(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                'PRIMARY KEY(id)',
            ]);

            // give it a FK to the elements table
            $this->addForeignKey(
                $this->db->getForeignKeyName('{{%products}}', 'id'),
                '{{%products}}', 'id', '{{%elements}}', 'id', 'CASCADE', null);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // if ($this->db->tableExists('{{%accompanies}}')) {
        //     $this->dropTable('{{%accompanies}}');
        // }
    }
}
