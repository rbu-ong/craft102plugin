<?php

namespace craft\products\controllers;

use products\Products;

use Craft;

use craft\helpers\DateTimeHelper;
use craft\helpers\Json;
use craft\helpers\UrlHelper;

use craft\web\Controller;
use craft\products\elements\Plugin;
use yii\base\UserException;
use yii\web\Response;

class DefaultController extends Controller
{
    /**
     * Display company data.
     *
     * @param array $variables
     * @return mixed
     * @throws HttpException
     */
    public function actionSaveProduct(array $variables = [])
    {

        $this->requirePostRequest();

        $entry = $this->_getEntryModel();
        // $entry = $this->_getEntryModel();
        $request = Craft::$app->getRequest();

         // Populate the entry with post data
        $this->_populateEntryModel($entry);
        
        Craft::$app->getElements()->saveElement($entry);

        Craft::$app->getSession()->setNotice(Craft::t('app', 'Entry saved.'));

        return $this->redirectToPostedUrl($entry);

        // $settings = $this->getPlugin()->getSettings();

        // if (empty($variables['company'])) {
        //     if (!empty($variables['accompanyId'])) {
        //         $variables['company'] = craft()->aCCompanies->find($variables['accompanyId']);
        //         if (!$variables['company'])
        //         {
        //             throw new HttpException(404);
        //         }
        //     } else {
        //         $variables['company'] = new ACCompaniesModel();
        //     }
        // }

        // $variables['title'] = $variables['company']->id ? $variables['company']->title : Craft::t('Create a new company');
        // $variables['tagGroupId'] =  $settings->tagGroupId;

        // // Set asset logo
        // $variables['assetId'] = $variables['company']->assetId;
        // $variables['elements'] = $variables['assetId'] ? [craft()->elements->getElementById($variables['assetId'])] : [];

        // // Set asset banner
        // $variables['bannerId'] = $variables['company']->bannerId;
        // $variables['bannerElements'] = $variables['bannerId'] ? [craft()->elements->getElementById($variables['bannerId'])] : [];

        // if ($variables['company']->properties['quick_links'])
        // {
        //     $variables['qLinks'] = craft()->aCCompanies->prepLinks($variables['company']->properties['quick_links']);
        // }

        // $variables['empty_slot'] = [
        //     'id' => 'slot',
        //     'name' => 'slot',
        //     'required' => 'true',
        //     'cols' => [
        //         'linktext' => [
        //             'heading' => 'Link Text',
        //             'type' => 'singleline'
        //         ],
        //         'linkurl' => [
        //             'heading' => 'link URL',
        //             'type' => 'singleline'
        //         ]
        //     ],
        //     'rows' => ''
        // ];

        // // Can't just use the entry's getCpEditUrl() because that might include the locale ID when we don't want it
        // $variables['baseCpEditUrl'] = 'eewebextras/accompanies/{id}';

        // // Set the "Continue Editing" URL
        // $variables['continueEditingUrl'] = $variables['baseCpEditUrl'] .
        //     (craft()->isLocalized() && craft()->getLanguage() != $variables['localeId'] ? '/' . $variables['localeId'] : '');

        // return $this->renderTemplate('products/edit', $variables);
    }

    /**
     * Fetches or creates an Entry.
     *
     * @return Entry
     * @throws NotFoundHttpException if the requested entry cannot be found
     */
    private function _getEntryModel(): Plugin
    {
        // $entryId = Craft::$app->getRequest()->getBodyParam('entryId');
        // $siteId = Craft::$app->getRequest()->getBodyParam('siteId');

        // if ($entryId) {
        //     $entry = Craft::$app->getEntries()->getEntryById($entryId, $siteId);

        //     if (!$entry) {
        //         throw new NotFoundHttpException('Entry not found');
        //     }
        // } else {
            $entry = new Plugin();
        //     $entry->sectionId = Craft::$app->getRequest()->getRequiredBodyParam('sectionId');

        //     if ($siteId) {
        //         $entry->siteId = $siteId;
        //     }
        // }

        return $entry;
    }

    /**
     * Populates an Entry with post data.
     *
     * @param Entry $entry
     *
     * @return void
     */
    private function _populateEntryModel(Plugin $entry)
    {
        $request = Craft::$app->getRequest();

        // Set the entry attributes, defaulting to the existing values for whatever is missing from the post data
        // $entry->typeId = Craft::$app->getRequest()->getBodyParam('typeId', $entry->typeId);
        // $entry->slug = Craft::$app->getRequest()->getBodyParam('slug', $entry->slug);
        // if (($postDate = Craft::$app->getRequest()->getBodyParam('postDate')) !== null) {
        //     $entry->postDate = DateTimeHelper::toDateTime($postDate) ?: null;
        // }
        // if (($expiryDate = Craft::$app->getRequest()->getBodyParam('expiryDate')) !== null) {
        //     $entry->expiryDate = DateTimeHelper::toDateTime($expiryDate) ?: null;
        // }
        // $entry->enabled = (bool)Craft::$app->getRequest()->getBodyParam('enabled', $entry->enabled);
        // $entry->enabledForSite = (bool)Craft::$app->getRequest()->getBodyParam('enabledForSite', $entry->enabledForSite);
        $entry->title = $request->getBodyParam('title', $entry->title);
        $entry->currency = $request->getBodyParam('currency', $entry->currency);
        $entry->price = $request->getBodyParam('price', $entry->price);

        // if (!$entry->typeId) {
        //     // Default to the section's first entry type
        //     $entry->typeId = $entry->getSection()->getEntryTypes()[0]->id;
        // }

        // $entry->fieldLayoutId = $entry->getType()->fieldLayoutId;
        // $fieldsLocation = Craft::$app->getRequest()->getParam('fieldsLocation', 'fields');
        // $entry->setFieldValuesFromRequest($fieldsLocation);

        // // Author
        // $authorId = Craft::$app->getRequest()->getBodyParam('author', ($entry->authorId ?: Craft::$app->getUser()->getIdentity()->id));

        // if (is_array($authorId)) {
        //     $authorId = $authorId[0] ?? null;
        // }

        // $entry->authorId = $authorId;

        // // Parent
        // if (($parentId = Craft::$app->getRequest()->getBodyParam('parentId')) !== null) {
        //     if (is_array($parentId)) {
        //         $parentId = reset($parentId) ?: '';
        //     }

        //     $entry->newParentId = $parentId ?: '';
        // }

        // // Revision notes
        // $entry->revisionNotes = Craft::$app->getRequest()->getBodyParam('revisionNotes');
    }

}