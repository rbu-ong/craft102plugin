<?php

namespace craft\accompanies\migrations;

use Craft;
use craft\db\Migration;

/**
 * m180109_044245_pluginmigrationfile migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Place migration code here...
        if (!$this->db->tableExists('{{%accompanies}}')) {
            // create the companies table
            $this->createTable('{{%accompanies}}', [
                'id' => $this->integer()->notNull(),
                'userId' => $this->integer()->notNull(),
                'description' => $this->text(),
                'accountSettings' => $this->text(),
                'properties' => $this->text(),
                'email' => $this->string(),
                'username' => $this->string(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                'PRIMARY KEY(id)',
            ]);

            // give it a FK to the elements table
            $this->addForeignKey(
                $this->db->getForeignKeyName('{{%accompanies}}', 'id'),
                '{{%accompanies}}', 'id', '{{%elements}}', 'id', 'CASCADE', null);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        if ($this->db->tableExists('{{%accompanies}}')) {
            $this->dropTable('{{%accompanies}}');
        }
    }
}
