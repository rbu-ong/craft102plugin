<?php
namespace craft\accompanies\services;

use Craft;

use craft\accompanies\elements\Plugin;

use yii\base\Component;
use yii\base\InvalidParamException;
use yii\db\Exception as DbException;
use craft\accompanies\models\CompanyModel as ACCompaniesModel;
use craft\accompanies\records\AccompaniesRecord;

class CompanyServices extends Component
{

    /**
     * @param array $criteria
     * @return ElementCriteriaModel
     */
    public function getElements($criteria = [])
    {
        return craft()->elements->getCriteria('ACCompanies', $criteria);
    }

    /**
     * Get company based on its email address.
     *
     * @param $email
     * @return mixed
     */
    public function getCompanyByEmail($email)
    {
        $elements = $this->getElements();

        foreach ($elements as $element)
        {
            if ($element->email == $email)
            {
                return $element;
            }
        }
    }
    
    public function getCompanyByEntrySlug($slug)
    {
        $articleCriteria = craft()->elements->getCriteria(ElementType::Entry, [
            'slug' => $slug
        ]);

        $articles = $articleCriteria->find();

        $userIds = [];
        foreach ($articles as $k => $article)
        {
            $userIds[] = $article->authorId;
        }

        $companyCriteria = $this->getElements([
            'userId' => $userIds
        ]);

        return $companyCriteria->first();
    }

    /**
     * Get company based on its username.
     *
     * @param $username
     * @return mixed
     */
    public function getCompanyByUsername($username)
    {
        $elements = $this->getElements();

        foreach ($elements as $element)
        {
            if ($element->username == $username)
            {
                return $element;
            }
        }
    }

    /**
     * Get the element based on the topic/tag.
     *
     * @param $criteria
     * @param $topic
     * @return array
     */
    public function getElementsByTopic($criteria, $topic)
    {
        $elements = $this->getElements($criteria);

        $results = [];

        foreach ($elements as $element)
        {
            if (in_array($topic->id, (array) $element->properties['topics']) && $element->accountSettings['businessType'] == '1')
            {
                $results[] = $element;
            }

        }

        return $results;
    }

    public function getPremiumCompanies($criteria)
    {
        $elements = $this->getElements($criteria);

        $results = [];

        foreach ($elements as $element)
        {
            if ($element->accountSettings['businessType'] == '1')
            {
                $results[] = $element;
            }

        }

        return $results;
    }

    /**
     * Returns plugin settings.
     *
     * @return BaseModel
     */
    public function getSettings()
    {
        return $this->plugin->getSettings();
    }

    public function getEntryTitle($slug)
    {
        $data['entry'] = $slug;
        if ($entry = craft()->aCCompanies->getEntry($data))
        {
            $type = ($entry->type == 'news') ? 'news' : $entry->type;

            return ucwords($entry->title . ' - ' . $entry->author->friendlyName . ' ' . $type);
        }
    }

    /**
     * Get tags based on tag's slug.
     *
     * @param $tag
     * @param string $group
     * @return mixed
     */
    public function getTags($tag, $group = 'accompanies')
    {
        $criteria = craft()->elements->getCriteria(ElementType::Tag);
        $criteria->slug = $tag;
        $criteria->group = $group;
        $criteria->order = 'title ASC';
        $element = $criteria->find();

        $element = ($element != null and is_array($element)) ? $element[0] : $element;
        return $element;
    }

    /**
     * Find in companies based on id.
     *
     * @param $id
     * @return BaseElementModel|null
     */
    public function find($id)
    {
        return Craft::$app->elements->getElementById($id);
    }

    /**
     * Create json for the quick links.
     *
     * @param $post
     * @return string
     */
    public function jsonifyLinks($post)
    {
        $links = [];
        foreach ($post as $k => $p)
        {
            if (strpos($k, '-t') !== false)
            {
                $parts = explode('-', $k);
                $linkNum = $parts[1];

                if (strpos($k, $linkNum) !== false)
                {
                    $i = (int) str_replace('t', '', $linkNum) - 1;
                    if ($p != '')
                    {
                        $links[ $i ][ $k ] = $p;
                    }
                }
            }
        }

        return json_encode($links);
    }

    /**
     * Prepare the quick links.
     *
     * @param $links
     * @return mixed
     */
    public function prepLinks($links)
    {
        $links = json_decode($links, true);
        foreach ($links as &$link)
        {
            foreach ($link as $k => $v)
            {
                if (strpos($k, 'heading-') !== false)
                {
                    $parts = explode('-t', $k);
                    $link['qLink_num'] = $parts[1];
                    $link['qLink_name'] = [
                        "label" => "Heading",
                        "name" => $k,
                        "class" => "nicetext",
                        "value" => $v
                    ];
                }

                if (strpos($k, 'slot-') !== false)
                {
                    $parts = explode('slot-', $k);

                    $link['slots'][] = [
                        "id"       => $k,
                        "name"     => $k,
                        "cols"     => [
                            'linktext'  => [
                                'heading' => 'Link Text',
                                'type'    => 'singleline'
                            ],
                            'linkurl' => [
                                'heading' => 'link URL',
                                'type'    => 'singleline'
                            ]
                        ],
                        'rows' => $v,
                        "ql_link" => $parts[1]
                    ];
                }
            }
        }

        return $links;
    }

    /**
     * Get the number of companies that has the same topic.
     *
     * @return array
     */
    public function getTopicsCount()
    {
        $companies = $this->getElements();
        $raw_interests = [];
        foreach ($companies as $company)
        {
            if ($company->accountSettings['businessType'] == '1' && $company->properties['topics'] != '')
            {
                foreach ($company->properties['topics'] as $interest)
                {
                    $tagId = (int) $interest;
                    $tag = craft()->tags->getTagById($tagId, '');
                    $raw_interests[$interest]['title'] = $tag->title;
                    $raw_interests[$interest]['slug'] = $tag->slug;
                    $raw_interests[$interest]['id'] = (int) $interest;
                    $raw_interests[$interest]['raw'][] = $interest;
                }
            }
        }
        $interests = [];
        foreach ($raw_interests as $raw_interest)
        {
            $interests[$raw_interest['id']]['title'] = $raw_interest['title'];
            $interests[$raw_interest['id']]['slug'] = $raw_interest['slug'];
            $interests[$raw_interest['id']]['count'] = count($raw_interest['raw']);
        }

        usort($interests, function($a, $b) {
            return $b['count'] - $a['count'];
        });

        return $interests;
    }

    /**
     * Create a user.
     *
     * @param $data
     * @return array|UserModel
     */
    public function createUser($data)
    {
        $user = new UserModel();
        $user->email = $data->email;
        $user->username = $data->username;
        $user->firstName = $data->title;

        if (!craft()->users->saveUser($user))
        {
            $errors = $user->getErrors();
            return $errors;
        }

        $userGroup = craft()->userGroups->getGroupByHandle('freeBusiness');

        if ($data->accountSettings['businessType'] == 1)
        {
            $userGroup = craft()->userGroups->getGroupByHandle('premiumBusiness');
        }

        $userId = $user->id;
        craft()->userGroups->assignUserToGroups($userId, $userGroup->id);

        return $user;
    }

    /**
     * Register a company based on a user.
     *
     * @param $userId
     * @param $data
     * @return bool
     */
    public function register($userId, $data)
    {
        $company = new ACCompaniesModel();

        $company->getContent()->title = $data['businessName'];
        $company->username = $data['username'];
        $company->email = $data['email'];
        $company->accountSettings['businessType'] = 0;
        $company->userId = $userId;

        $result = $this->save($company);

       return $result;
    }

    /**
     * Save a company.
     *
     * @param ACCompaniesModel $model
     * @return bool
     * @throws Exception
     * @throws \Exception
     */
    public function save(ACCompaniesModel $model)
    {
        // $isNew = !$model->id;
        $isNew = 1;

        // if (!$isNew)
        // {
            // $record = ACCompaniesRecord::model()->findById($model->id);
            // if (!$record)
            // {
            //     throw new Exception(Craft::t('No company item exists with the ID "{id}"', [
            //         'id' => $model->id
            //     ]));
            // }
        // }
        // else
        // {
            $record = new AccompaniesRecord();
        // }

        // print_r( $record );
        // die;

        $record->setAttributes($model->getAttributes(), false);

        if (!$record->validate())
        {
            // attach errors to model
            $model->addErrors($record->getErrors());

            return false;
        }

        if (!$model->hasErrors())
        {
            $transaction = Craft::$app->getDb()->beginTransaction();
            try
            {   
                if ($isNew)
                {
                    $record->id = $model->id;
                    $record->userId = 1;
                    // $record->elementId = $model->id;
                }

                // save to plugin table
                $record->save(false);

                if ($transaction !== null)
                {
                    $transaction->commit();
                }

                // Fire an 'onSaveCompany' event
                // $this->onSaveCompany(new Event($this, [
                //     'company' => $model,
                //     'isNew' => $isNew
                // ]));

                return true;
            }
            catch (\Exception $e)
            {

                echo $e;
                die;

                if ($transaction !== null)
                {
                    $transaction->rollback();
                }

                throw $e;
            }
        }
    }

    /**
     * Delete a company.
     *
     * @param $companies
     * @return bool
     * @throws \Exception
     */
    public function delete($companies)
    {
        if (!$companies)
        {
            return false;
        }

        $transaction = craft()->db->getCurrentTransaction() === null ? craft()->db->beginTransaction() : null;

        try
        {
            if (!is_array($companies))
            {
                $companies = array($companies);
            }

            $companyIds = array();

            foreach ($companies as $company)
            {
                // Fire an 'onBeforeDeleteCompany' event
                $event = new Event($this, array(
                    'company' => $company
                ));

                $this->onBeforeDeleteCompany($event);

                if ($event->performAction)
                {

                    $companyIds[] = $company->id;
                }
            }

            if ($companyIds)
            {
                // Delete 'em
                $success = craft()->elements->deleteElementById($companyIds);
            }
            else
            {
                $success = false;
            }

            if ($transaction !== null)
            {
                $transaction->commit();
            }
        }
        catch (\Exception $e)
        {
            if ($transaction !== null)
            {
                $transaction->rollback();
            }

            throw $e;
        }

        if ($success)
        {
            foreach ($companies as $company)
            {
                // Fire an 'onDeleteCompany' event
                $this->onDeleteCompany(new Event($this, array(
                    'company' => $company
                )));
            }

            return true;
        }
        else
        {
            return false;
        }
    }

    /**
     * Check the validity of the username.
     *
     * @param $input
     * @param string $pattern
     * @return bool
     */
    public function isValidUsername($input, $pattern = '/^[a-z0-9\-]+$/')
    {
        if (preg_match($pattern, $input)) return true;
        else return false;
    }

    public function isBusiness(array $variables)
    {
        $business = $this->getCompanyByUsername($variables['username']);
        if ($business == null) return false;
        else return true;
    }

    /**
     * Fires an 'onBeforeDeleteCompany' event.
     *
     * @param Event $event
     *
     * @return null
     */
    public function onBeforeDeleteCompany(Event $event)
    {
        $this->raiseEvent('onBeforeDeleteCompany', $event);
    }

    /**
     * Fires an 'onDeleteCompany' event.
     *
     * @param Event $event
     *
     * @return null
     */
    public function onDeleteCompany(Event $event)
    {
        $this->raiseEvent('onDeleteCompany', $event);
    }

    /**
     * Before saving company in HUD display
     *
     * @param $postData
     * @return ACCompaniesModel
     * @throws Exception
     */
    public function sanitizeData($postData)
    {
        $namespace = $postData['namespace'];

        if (isset($postData[$namespace]['companyId']) && $companyId = $postData[$namespace]['companyId'])
        {
            $company = craft()->aCCompanies->find($companyId);
            if (!$company)
            {
                throw new Exception(Craft::t('No company exists with ID --> "{id}"', [
                    'id' => $company
                ]));
            }
        }
        else
        {
            $company = new ACCompaniesModel();
        }

        $accountSettings = [
            'businessType' => 0,
            'hasCareer' => 0,
            'displayUser' => $postData[$namespace]['accountSettings']['displayUser'] ?: 0,
        ];

        $company->getContent()->title = $postData[$namespace]['title'];
        $company->username = $postData[$namespace]['username'];
        $company->email = $postData[$namespace]['email'];
        $company->description = '';
        $company->enabled = true;
        $company->accountSettings = $accountSettings;

        $assetId = (isset($postData[$namespace]['assetId']) && $postData[$namespace]['assetId']) ? $postData[$namespace]['assetId'][0] : null;
        $company->assetId = $assetId;

        $topics = [];
        $settings = [];
        $qLinks = '';

        $company->properties = ['topics' => $topics, 'settings' => $settings, 'quick_links' => $qLinks];

        return $company;
    }

    /**
     * Fires an 'onSaveCompany' event.
     *
     * @param Event $event
     *
     * @return null
     */
    public function onSaveCompany(Event $event)
    {
        $this->raiseEvent('onSaveCompany', $event);
    }

    /** Prepare data in properties.
     *
     * @param $company
     * @return array
     */
    public function prepProperties($company)
    {
        $properties = [];

        if (isset($company->properties['settings']['entries']) && $company->properties['settings']['entries'] != '' )
        {
            foreach ($company->properties['settings']['entries'] as $key => $entry)
            {
                $properties['entries'][$key] = $entry;
            }
        }

        if (isset($company->properties['settings']['quick_links']['title']) && $company->properties['settings']['quick_links']['title'] != '')
        {
            $properties['quick_links']['title'] = $company->properties['settings']['quick_links']['title'] ?: Craft::t('Quick Links to') . $company->title;
        }

        return $properties;
    }

    /**
     * Check if the company belongs to a premium category.
     *
     * @param array $variables
     * @return bool
     */
    public function isPremium(array $variables = [])
    {
        $company = $this->getCompanyByUsername($variables['username']);
        if ($company)
        {
            $isPremium = $company->accountSettings['businessType'];

            if ($isPremium) return true;
            else return false;
        }
    }

    public function getEntry($data)
    {
        $criteria = craft()->elements->getCriteria(ElementType::Entry);
        $criteria->slug = $data['entry'];
        $element = $criteria->find();
        if (count($element) > 0) return $element[0];
        else return null;
    }

    public function getSiteTemplate()
    {
        $settings = $this->getSettings();
        return preg_replace('/{theme}/', craft()->config->get('theme'), $settings->siteTemplate);
    }

    public function getEntryPath(Event $event)
    {
        if ($entry = $event->params['params']['variables']['entry'])
        {
            if (!$entry instanceof EntryModel)
            {
                return false;
            }

            if ($entry->type->handle == 'articles' OR $entry->type->handle == 'news' OR $entry->type->handle == 'company_news')
            {
                if ($author = $event->params['params']['variables']['entry']->author)
                {
                    $isPremium = $this->isPremium(['username' => $author->username]);
                    if ($isPremium)
                    {
                        $company = craft()->aCCompanies->getCompanyByUsername($author->username);
                        $event->params['params']['variables']['company'] = $company;
                        $event->params['params']['variables']['entry'] = $entry;
                        $event->params['params']['variables']['properties'] = craft()->aCCompanies->prepProperties($company);
                        $event->params['params']['template'] = $this->getSiteTemplate() . '/_entry';
                    }
                    else
                    {
                        if ($entry->type->handle == 'articles')
                        {
                            $settings = '{theme}/articles/articles/_entry';
                        }
                        elseif ($entry->type->handle == 'news' or $entry->type->handle == 'company_news') 
                        {
                            $settings = '{theme}/articles/news/_entry';
                        }
                        $siteTemplate = preg_replace('/{theme}\//', craft()->config->get('theme'), $settings);
                        $event->params['params']['template'] = $siteTemplate;
                    }
                }
            }
        }

        return $event;
    }

    public function getRegions($companyId)
    {
        $jobs = craft()->aCJobs->getAll()->find();

        $jobsWithLocations = [];
        foreach ($jobs as $job)
        {

            if ($job->locationId)
            {
                $location = craft()->aCLocations->getLocationById($job->locationId);

                if ($job->companyId == $companyId)
                {

                    $jobsWithLocations[$job->locationId]['regionId'] = $location->regionId;
                    $jobsWithLocations[$job->locationId]['job'][] = $job->title;
                }
            }
        }

        $locationRegions = craft()->aCLocations_region->getAll(['order' => 'title ASC'])->find();
        $regions = [];
        foreach ($locationRegions as $key => $region)
        {
            $regions[$key]['title'] = $region->title;
            $regions[$key]['slug'] = $region->slug;
            $regions[$key]['count'] = 0;
            foreach ($jobsWithLocations as $jobsWithLocation)
            {

                if ($jobsWithLocation['regionId'] == $region->id)
                {
                    $regions[$key]['count'] = count($jobsWithLocation['job']);
                }
            }
        }



        return $regions;
    }

    public function getJobs($variables, $companyId)
    {
        if (isset($variables['browse']) and $variables['browse'] == 'level')
        {
            $term = ucwords(str_replace('-', ' ', $variables['term']));
            $criteria = [
                'companyId' => $companyId,
                'level' => $term
            ];
        }
        elseif (isset($variables['browse']) and $variables['browse'] == 'regions')
        {
            $region = craft()->aCLocations_region->getAll(['slug' => $variables['term']])->first();
            $criteria = [
                'companyId' => $companyId,
                'regionId' => $region->id
            ];
        }
        else
        {
            $criteria = [
                'companyId' => $companyId
            ];
        }

        if (isset($variables['tag']) or (isset($variables['browse']) and $variables['browse'] == 'tag'))
        {
            if ($variables['browse'] == 'tag') $term = $variables['term'];
            else $term = $variables['tag'];

            $tag = craft()->aCJobs->getTags(['slug' => $term])->first();
            $criteria = array_merge($criteria, ['tagId' => $tag->id]);

        }

        $jobs = craft()->aCJobs->getAll($criteria);

        return $jobs;

    }
}