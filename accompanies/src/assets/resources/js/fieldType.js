$(function () {
    generateSelectBoxField();

    $('.generate').on('click', function () {
        generateSelectBoxField();
    });

});

var generateSelectBoxField = function () {
    $('.spinner').css({display: 'inline-block'});

    var $options = getCheckBoxValues();

    if($options.length == 0) {
        $('.generate-buttons .warning').show().html("Please select a business type.");
        $('.companies-box').hide();
        $('.spinner').css({display: 'none'});
        return false;
    }

    generateOptions($options);


};

var getCheckBoxValues = function () {
    var $companyFieldContainer = $('.company-settings');
    var $checkBoxes = $companyFieldContainer.find('input[type="checkbox"]');
    var options = [];

    $checkBoxes.each(function (row, obj) {
        var $name = obj.name;
        if($name.indexOf('business_type') != -1) {
            if(obj.checked) {
                options.push(obj.value);
            }
        }
    });

    return options;
};

var generateOptions = function ($options) {
    var businessType = $options.join(',');
    var data = {type: businessType};
    var token = $('.hidden-token input[type="hidden"]');
    data['CRAFT_CSRF_TOKEN'] = token.val();
    var $selectOptions = [];

    $.ajax({
        method: 'POST',
        url: '/admin/accompany/generate',
        data: data
    })
        .success(function(data) {
            var obj = jQuery.parseJSON(data);

            for(var o in obj) {
                $selectOptions.push('<option value="' + obj[o].option + '">' + obj[o].label + '</option>');
            }

            setTimeout(function () {
                $('.spinner').css({display: 'none'});
                $('.companies-box').show();
                $('.generate-buttons .warning').hide();

                $('.company-selectbox select').html($selectOptions.join(''));
            }, 1000);
        })
        .error(function(data) {
            console.log('error', data);
        });
};