(function($){

    Craft.CompanyQuickLinks = Garnish.Base.extend(
        {
            id: null,
            inputNamePrefix: null,
            inputIdPrefix: null,

            showingAddBlockMenu: false,
            addBlockBtnGroupWidth: null,
            addBlockBtnContainerWidth: null,

            $container: null,
            $blockContainer: null,
            $addBlockBtnContainer: null,
            $addBlockBtnGroup: null,
            $addBlockBtnGroupBtns: null,
            $addSlotBtn: null,
            $delSlotBtn: null,

            blockSort: null,
            blockSelect: null,
            totalNewBlocks: 0,
            qLinkNum: 0,
            slotLabel: 'slot',

            init: function (id, inputNamePrefix) {
                this.id = id;
                this.inputNamePrefix = inputNamePrefix;
                this.inputIdPrefix = Craft.formatInputId(this.inputNamePrefix);

                this.$container = $('#'+this.id);
                this.$blockContainer = this.$container.children('.blocks');
                this.$addBlockBtnContainer = this.$container.children('.buttons');
                this.$addBlockBtnGroup = this.$addBlockBtnContainer.children('.btngroup');
                this.$addBlockBtnGroupBtns = this.$addBlockBtnGroup.children('.btn');
                this.$addBlockMenuBtn = this.$addBlockBtnContainer.children('.menubtn');
                this.$addSlotBtn = $('.btn-add-slot');
                this.$delSlotBtn = $('.btn-del-slot');
                this.$cloneSlotBtn = $('.btn-clone-slot');

                this.setNewBlockBtn();

                var $blocks = this.$blockContainer.children(),
                    collapsedBlocks = Craft.CompanyQuickLinks.getCollapsedBlockIds();

                this.blockSort = new Garnish.DragSort($blocks, {
                    handle: '> .actions > .move',
                    axis: 'y',
                    filter: $.proxy(function()
                    {
                        // Only return all the selected items if the target item is selected
                        if (this.blockSort.$targetItem.hasClass('sel'))
                        {
                            return this.blockSelect.getSelectedItems();
                        }
                        else
                        {
                            return this.blockSort.$targetItem;
                        }
                    }, this),
                    collapseDraggees: true,
                    magnetStrength: 4,
                    helperLagBase: 1.5,
                    helperOpacity: 0.9,
                    onSortChange: $.proxy(function() {
                        this.blockSelect.resetItemOrder();
                    }, this)
                });

                this.blockSelect = new Garnish.Select(this.$blockContainer, $blocks, {
                    multi: true,
                    vertical: true,
                    handle: '> .checkbox, > .titlebar',
                    checkboxMode: true
                });

                for (var i = 0; i < $blocks.length; i++)
                {
                    var $block = $($blocks[i]),
                        id = $block.data('id');

                    // Is this a new block?
                    var newMatch = (typeof id == 'string' && id.match(/new(\d+)/));

                    if (newMatch && newMatch[1] > this.totalNewBlocks)
                    {
                        this.totalNewBlocks = parseInt(newMatch[1]);
                    }

                    var block = new CompanyQuickLinksBlock(this, $block);

                    if (block.id && $.inArray(''+block.id, collapsedBlocks) != -1)
                    {
                        block.collapse();
                    }
                }

                this.removeListener(this.$addBlockBtnGroupBtns, 'click');
                this.addListener(this.$addBlockBtnGroupBtns, 'click', function(ev)
                {
                    var type = $(ev.target).data('type');
                    this.addBlock(type, this.$container);
                });

                this.removeListener(this.$addSlotBtn, 'click');
                this.addListener(this.$addSlotBtn, 'click', function(ev)
                {
                    ev.preventDefault();
                    this.addSlot($(ev.target));
                });

                this.removeListener(this.$delSlotBtn, 'click');
                this.addListener(this.$delSlotBtn, 'click', function(ev)
                {
                    ev.preventDefault();
                    this.deleteSlot($(ev.target));
                });

                this.removeListener(this.$cloneSlotBtn, 'click');
                this.addListener(this.$cloneSlotBtn, 'click', function(ev)
                {
                    ev.preventDefault();
                    this.cloneSlot($(ev.target));
                });

                new Garnish.MenuBtn(this.$addBlockMenuBtn,
                    {
                        onOptionSelect: $.proxy(function(option)
                        {
                            var type = $(option).data('type');
                            this.addBlock(type);
                        }, this)
                    });


                this.addListener(this.$container, 'resize', function(){});
            },

            setNewBlockBtn: function()
            {
                // Do we know what the button group width is yet?
                if (!this.addBlockBtnGroupWidth)
                {
                    this.addBlockBtnGroupWidth = this.$addBlockBtnGroup.width();

                    if (!this.addBlockBtnGroupWidth)
                    {
                        return;
                    }
                }

                // Only check if the container width has resized
                if (this.addBlockBtnContainerWidth !== (this.addBlockBtnContainerWidth = this.$addBlockBtnContainer.width()))
                {
                    if (this.addBlockBtnGroupWidth > this.addBlockBtnContainerWidth)
                    {
                        if (!this.showingAddBlockMenu)
                        {
                            this.$addBlockBtnGroup.addClass('hidden');
                            this.$addBlockMenuBtn.removeClass('hidden');
                            this.showingAddBlockMenu = true;
                        }
                    }
                    else
                    {
                        if (this.showingAddBlockMenu)
                        {
                            this.$addBlockMenuBtn.addClass('hidden');
                            this.$addBlockBtnGroup.removeClass('hidden');
                            this.showingAddBlockMenu = false;

                            // Because Safari is awesome
                            if (navigator.userAgent.indexOf('Safari') !== -1)
                            {
                                Garnish.requestAnimationFrame($.proxy(function() {
                                    this.$addBlockBtnGroup.css('opacity', 0.99);

                                    Garnish.requestAnimationFrame($.proxy(function() {
                                        this.$addBlockBtnGroup.css('opacity', '');
                                    }, this));
                                }, this));
                            }
                        }
                    }
                }

            },

            updateAddBlockBtn: function()
            {
                this.$addBlockBtnGroup.removeClass('disabled');
                this.$addBlockMenuBtn.removeClass('disabled');

                for (var i = 0; i < this.blockSelect.$items.length; i++)
                {
                    var block = this.blockSelect.$items.eq(i).data('block');

                    if (block)
                    {
                        block.$actionMenu.find('a[data-action=add]').parent().removeClass('disabled');
                    }
                }
            },

            addBlock: function(type, $insertBefore)
            {
                this.totalNewBlocks++;
                this.qLinkNum = this.findLargestQLinkNum() + 1;

                var id = 'new'+this.totalNewBlocks;

                var html =
                    '<div class="matrixblock qlinks-container" data-id="'+id+'">' +
                    '<div class="titlebar dc-titlebar">' +
                    '<div class="blocktype"><span class="link icon"></span> <span class="light-gray">[new] '+this.totalNewBlocks+'</span></div>' +
                    '<div class="preview"></div>' +
                    '</div>' +
                    '<div class="actions">' +
                    '<div class="status off" title="'+Craft.t('Disabled')+'"></div>' +
                    '<a class="settings icon menubtn" title="'+Craft.t('Actions')+'" role="button"></a> ' +
                    '<div class="menu">' +
                    '<ul class="padded">' +
                    '<li><a data-icon="collapse" data-action="collapse">'+Craft.t('Collapse')+'</a></li>' +
                    '<li class="hidden"><a data-icon="expand" data-action="expand">'+Craft.t('Expand')+'</a></li>' +
                    '<li><a data-icon="wand" data-action="clone" data-qlinks-num="'+this.qLinkNum+'">'+Craft.t('Clone Quick Links')+'</a></li>' +
                    '<li><a data-icon="+" data-action="add" data-type="qlinks">'+Craft.t('Add Quick Links above')+'</a></li>' +
                    '<li><hr class="padded"/></li>' +
                    '<li><a data-icon="remove" data-action="delete">'+Craft.t('Delete')+'</a></li>' +
                    '</ul>' +
                    '</div>' +
                    '<a class="move icon" title="'+Craft.t('Reorder')+'" role="button"></a>' +
                    '</div>' +
                    '</div>';

                var $block = $(html);

                if ($insertBefore)
                {
                    $block.insertBefore($insertBefore);
                }
                else
                {
                    $block.appendTo(this.$blockContainer);
                }

                var $fieldsContainer = $('<div class="slots-container fields" id="slots-container-t'+this.qLinkNum+'"/>').appendTo($block),
                    $qLinkTemplate = this.getQLinkTemplate(this.qLinkNum, '1');

                $block.css('height', 'auto');
                $block.removeClass('collapsed');

                $qLinkTemplate.appendTo($fieldsContainer);

                $fieldsContainer.css({
                    'display':'block',
                    'opacity': 1
                });

                this.removeListener($('.btn-add-slot'), 'click');
                this.addListener($('.btn-add-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.addSlot($(ev.target));
                    this.addSlot($(ev.target));
                });

                this.removeListener($('.btn-del-slot'), 'click');
                this.addListener($('.btn-del-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.deleteSlot($(ev.target));
                });

                this.removeListener($('.btn-clone-slot'), 'click');
                this.addListener($('.btn-clone-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.cloneSlot($(ev.target));
                });

                var parts = this.slotLabel.split('-t'),
                    slotNum = 1;

                if (parts[1] != undefined) {
                    var parts2 = parts[1].split('-s');

                    slotNum = (parts2[0] == this.qLinkNum) ? parts2[1] : 1;
                }

                this.slotLabel = parts[0] + "-t" + this.qLinkNum + "-s" + slotNum;

                if (Craft.CompanyQuickLinks.debug) Garnish.log(this.slotLabel);

                new Craft.EditableTable(
                    this.slotLabel,
                    this.slotLabel,
                    Craft.CompanyQuickLinks.$emptySlot.cols
                );

                // Animate the block into position
                $block.css(this.getHiddenBlockCss($block)).velocity({
                    opacity: 1,
                    'margin-bottom': 10
                }, 'fast', $.proxy(function()
                {
                    $block.css('margin-bottom', '');
                    Craft.initUiElements($fieldsContainer);
                    new CompanyQuickLinksBlock(this, $block);

                    this.blockSort.addItems($block);
                    this.blockSelect.addItems($block);
                    this.updateAddBlockBtn();

                    Garnish.requestAnimationFrame(function()
                    {
                        // Scroll to the block
                        Garnish.scrollContainerToElement($block);
                    });
                }, this));
            },

            cloneQLinks: function(qLinkNum) {
                this.totalNewBlocks++;
                this.qLinkNum = this.findLargestQLinkNum() + 1;

                var cloneQLinksTemplate = $('#slots-container-t'+qLinkNum).html(),
                    qLinkNamePattern = new RegExp('qlink_header-t'+qLinkNum),
                    wpDivIdPattern = new RegExp('wp_div_id-t'+qLinkNum, "g"),
                    addSlotPattern = new RegExp('btn-add-slot-t'+qLinkNum),
                    qLinkSlotPattern = new RegExp("-t"+qLinkNum+"-s", "g"),
                    id = 'new'+this.totalNewBlocks;

                cloneQLinksTemplate = cloneQLinksTemplate
                    .replace(qLinkNamePattern, 'qlink_header-t'+this.qLinkNum)
                    .replace(wpDivIdPattern, 'wp_div_id-t'+this.qLinkNum)
                    .replace(qLinkSlotPattern, '-t'+this.qLinkNum+'-s')
                    .replace(addSlotPattern, 'btn-add-slot-t'+this.qLinkNum);

                var html =
                    '<div class="matrixblock qlinks-container" data-id="'+id+'">' +
                    '<div class="titlebar dc-titlebar">' +
                    '<div class="blocktype"><span class="link icon"></span> <span class="light-gray">[new] '+this.totalNewBlocks+'</span></div>' +
                    '<div class="preview"></div>' +
                    '</div>' +
                    '<div class="actions">' +
                    '<div class="status off" title="'+Craft.t('Disabled')+'"></div>' +
                    '<a class="settings icon menubtn" title="'+Craft.t('Actions')+'" role="button"></a> ' +
                    '<div class="menu">' +
                    '<ul class="padded">' +
                    '<li><a data-icon="collapse" data-action="collapse">'+Craft.t('Collapse')+'</a></li>' +
                    '<li class="hidden"><a data-icon="expand" data-action="expand">'+Craft.t('Expand')+'</a></li>' +
                    '<li><a data-icon="wand" data-action="clone" data-qlink-num="'+this.qLinkNum+'">'+Craft.t('Clone Quick Links')+'</a></li>' +
                    '<li><a data-icon="+" data-action="add" data-type="qlinks">'+Craft.t('Add Quick Links above')+'</a></li>' +
                    '<li><hr class="padded"/></li>' +
                    '<li><a data-icon="remove" data-action="delete">'+Craft.t('Delete')+'</a></li>' +
                    '</ul>' +
                    '</div>' +
                    '<a class="move icon" title="'+Craft.t('Reorder')+'" role="button"></a> ' +
                    '</div>' +
                    '</div>';

                var $block = $(html);

                $block.appendTo(this.$blockContainer);

                var $fieldsContainer = $('<div class="slots-container fields" id="slots-container-t'+this.qLinkNum+'"/>').appendTo($block);

                $block.css('height', 'auto');
                $block.removeClass('collapsed');

                $(cloneQLinksTemplate).appendTo($fieldsContainer);

                $fieldsContainer.css({
                    'display':'block',
                    'opacity': 1
                });

                this.removeListener($('.btn-add-slot'), 'click');
                this.addListener($('.btn-add-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.addSlot($(ev.target));
                });

                this.removeListener($('.btn-del-slot'), 'click');
                this.addListener($('.btn-del-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.deleteSlot($(ev.target));
                });

                this.removeListener($('.btn-clone-slot'), 'click');
                this.addListener($('.btn-clone-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.cloneSlot($(ev.target));
                });

                var slots = $fieldsContainer.find('.slot-wrapper');

                for (var i in slots)
                {
                    if ( !isNaN(i) )
                    {
                        var slotId = slots[i].id,
                            parts = slotId.split('slot-wrapper-');

                        this.slotLabel = 'slot-'+parts[1];

                        if (Craft.CompanyQuickLinks.debug) Garnish.log(this.slotLabel);

                        new Craft.EditableTable(
                            this.slotLabel,
                            this.slotLabel,
                            Craft.CompanyQuickLinks.$emptySlot.cols
                        );
                    }
                }

                // Animate the block into position
                $block.css(this.getHiddenBlockCss($block)).velocity({
                    opacity: 1,
                    'margin-bottom': 10
                }, 'fast', $.proxy(function()
                {
                    $block.css('margin-bottom', '');
                    Craft.initUiElements($fieldsContainer);
                    new CompanyQuickLinksBlock(this, $block);

                    this.blockSort.addItems($block);
                    this.blockSelect.addItems($block);
                    this.updateAddBlockBtn();

                    Garnish.requestAnimationFrame(function()
                    {
                        // Scroll to the block
                        Garnish.scrollContainerToElement($block);
                    });
                }, this));
            },

            addSlot: function($slot)
            {
                var parts = this.slotLabel.split('-t'),
                    slotId = $slot[0].id,
                    idParts = slotId.split('-t'),
                    qLinkNum = idParts[1],
                    slotNum = parseInt($('#btn-add-slot-t'+qLinkNum).data('slotNum')) + 1;

                if (parts[1] != undefined) {
                    var parts2 = parts[1].split('-s');

                    if (parts2[1] != undefined && qLinkNum == parts2[0]) {
                        slotNum = parseInt(parts2[1])+1;
                    } else {
                        slotNum = ($slot.data('slot') != undefined) ? parseInt($slot.data('slot')) + 1 : slotNum;
                    }
                }

                $slot.data('slot', slotNum);
                this.slotLabel = parts[0] + "-t" + qLinkNum + "-s" + slotNum;

                var $slotTemplate = this.getSlotTemplate(qLinkNum, slotNum);

                if (Craft.CompanyQuickLinks.debug) Garnish.log(this.slotLabel);

                $slotTemplate.insertBefore($slot.parent());

                $slotTemplate.css({
                    opacity: 0
                }).velocity({
                    opacity: 1
                }, 'fast');

                new Craft.EditableTable(
                    this.slotLabel,
                    this.slotLabel,
                    Craft.CompanyQuickLinks.$emptySlot.cols
                );

                this.removeListener($('.btn-del-slot'), 'click');
                this.addListener($('.btn-del-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.deleteSlot($(ev.target));
                });

                this.removeListener($('.btn-clone-slot'), 'click');
                this.addListener($('.btn-clone-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.cloneSlot($(ev.target));
                });
            },

            deleteSlot:function($slot)
            {
                if (confirm(Craft.t('Are you sure you want to delete the slot?')))
                {
                    var slotId = $slot[0].id,
                        idParts = slotId.split('del-slot-'),
                        $slotWrapper = $('#slot-wrapper-'+idParts[1]);

                    $slotWrapper.css({
                        opacity: 1
                    }).velocity({
                        opacity: 0
                    }, 'fast', function(){
                        $slotWrapper.remove();
                    });
                }
            },

            cloneSlot:function($slot)
            {
                var slotId = $slot[0].id,
                    idParts = slotId.split('-t'),
                    idParts2 = idParts[1].split('-s'),
                    qLinkNum = idParts2[0],
                    slotNum = idParts2[1],
                    cloneSlotNum = this.findLargestSlotNum(qLinkNum) + 1,
                    cloneTemplate = $('#slot-wrapper-t'+qLinkNum+'-s'+slotNum).html(),
                    pattern = new RegExp("-t"+qLinkNum+"-s"+slotNum, "g"),
                    $slotBtn = $('#btn-add-slot-t'+qLinkNum),
                    $slotWrapper = $('<div class="slot-wrapper" id="slot-wrapper-t'+qLinkNum+'-s'+cloneSlotNum+'"/>');

                cloneTemplate = cloneTemplate
                    .replace(pattern, '-t'+qLinkNum+'-s'+cloneSlotNum);

                $slotBtn.data('slot', slotNum);
                this.slotLabel = "slot" + "-t" + qLinkNum + "-s" + cloneSlotNum;

                if (Craft.CompanyQuickLinks.debug) Garnish.log(this.slotLabel);

                $slotWrapper
                    .append($(cloneTemplate))
                    .insertBefore($slotBtn.parent())
                    .css({
                        opacity: 0
                    }).velocity({
                    opacity: 1
                },'fast');

                new Craft.EditableTable(
                    this.slotLabel,
                    this.slotLabel,
                    Craft.CompanyQuickLinks.$emptySlot.cols
                );

                this.removeListener($('.btn-del-slot'), 'click');
                this.addListener($('.btn-del-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.deleteSlot($(ev.target));
                });

                this.removeListener($('.btn-clone-slot'), 'click');
                this.addListener($('.btn-clone-slot'), 'click', function(ev)
                {
                    ev.preventDefault();
                    this.cloneSlot($(ev.target));
                });
            },

            getHiddenBlockCss: function($block)
            {
                return {
                    opacity: 0,
                    marginBottom: -($block.outerHeight())
                };
            },

            getQLinkTemplate: function(qLinkNum, slotNum)
            {
                var qLinkTemplate = '<div class="field"><div class="heading"><label>Heading</label></div><div class="input ltr"><input class="text nicetext fullwidth" type="text" name="heading-t{qLinkNum}" autocomplete="off"></div></div><table id="slot-t{qLinkNum}-s{slotNum}" class="shadow-box editable"> <thead> <tr> <th scope="col" class="header">Link Text</th> <th scope="col" class="header">Link URL</th> <th class="header" colspan="2"></th> </tr></thead> <tbody></tbody> </table> <div class="btn add icon">Add a row</div></div>';

                qLinkTemplate = qLinkTemplate
                    .replace(/{qLinkNum}/g, qLinkNum)
                    .replace(/{slotNum}/g, slotNum);

                return $(qLinkTemplate);
            },

            getSlotTemplate: function (qLinkNum, slotNum)
            {
                var slotTemplate = '<div class="slot-wrapper" id="slot-wrapper-t{qLinkNum}-s{slotNum}"><table id="slot-t{qLinkNum}-s{slotNum}" class="shadow-box editable"><thead><tr><th scope="col" class="header">Link Text</th><th scope="col" class="header">Link URL</th><th class="header" colspan="2"></th></tr></thead><tbody></tbody></table><div class="btn add icon">Add a row</div></div>';

                slotTemplate = slotTemplate
                    .replace(/{qLinkNum}/g, qLinkNum)
                    .replace(/{slotNum}/g, slotNum);

                return $(slotTemplate);
            },

            findLargestQLinkNum: function () {
                var $slots = $('.slots-container'),
                    qLinks = [];

                for (var i in $slots)
                {
                    if ( !isNaN(i) )
                    {
                        var slotId = $slots[i].id;

                        if(slotId != undefined)
                        {
                            var parts = slotId.split('-t');
                            qLinks.push(parseInt(parts[1]));
                        }
                    }
                }

                return qLinks.length ? Math.max.apply(Math, qLinks) : 0;
            },

            findLargestSlotNum: function (qLinkNum) {
                var $slotContainer = $('#slots-container-t'+qLinkNum),
                    slots = $slotContainer.find('.slot-wrapper'),
                    slotNums = [];

                for (var i in slots)
                {
                    if ( !isNaN(i) )
                    {
                        var slotId = slots[i].id;

                        if(slotId != undefined)
                        {
                            var parts = slotId.split('-t'),
                                parts2 = parts[1].split('-s');

                            slotNums.push(parseInt(parts2[1]));
                        }
                    }
                }

                return slotNums.length ? Math.max.apply(Math, slotNums) : 0;
            }
        },
        {
            $emptySlot: {},
            debug: false,

            collapsedBlockStorageKey: 'Craft-' + Craft.siteUid + '.CompanyQuickLinks.collapsedBlocks',

            getCollapsedBlockIds: function () {
                if (typeof localStorage[Craft.CompanyQuickLinks.collapsedBlockStorageKey] == 'string') {
                    return Craft.filterArray(localStorage[Craft.CompanyQuickLinks.collapsedBlockStorageKey].split(','));
                }
                else {
                    return [];
                }
            },

            setCollapsedBlockIds: function (ids) {
                localStorage[Craft.CompanyQuickLinks.collapsedBlockStorageKey] = ids.join(',');
            },

            rememberCollapsedBlockId: function (id) {
                if (typeof Storage !== 'undefined') {
                    var collapsedBlocks = Craft.CompanyQuickLinks.getCollapsedBlockIds();

                    if ($.inArray('' + id, collapsedBlocks) == -1) {
                        collapsedBlocks.push(id);
                        Craft.CompanyQuickLinks.setCollapsedBlockIds(collapsedBlocks);
                    }
                }
            },

            forgetCollapsedBlockId: function (id) {
                if (typeof Storage !== 'undefined') {
                    var collapsedBlocks = Craft.CompanyQuickLinks.getCollapsedBlockIds(),
                        collapsedBlocksIndex = $.inArray('' + id, collapsedBlocks);

                    if (collapsedBlocksIndex != -1) {
                        collapsedBlocks.splice(collapsedBlocksIndex, 1);
                        Craft.CompanyQuickLinks.setCollapsedBlockIds(collapsedBlocks);
                    }
                }
            }

        });

    var CompanyQuickLinksBlock = Garnish.Base.extend(
        {
            matrix: null,
            $container: null,
            $titlebar: null,
            $fieldsContainer: null,
            $previewContainer: null,
            $actionMenu: null,
            $collapsedInput: null,

            isNew: null,
            id: null,

            collapsed: false,

            init: function(matrix, $container)
            {
                this.matrix = matrix;
                this.$container = $container;
                this.$titlebar = $container.children('.titlebar');
                this.$previewContainer = this.$titlebar.children('.preview');
                this.$fieldsContainer = $container.children('.fields');

                this.$container.data('block', this);

                this.id    = this.$container.data('id');
                this.isNew = (!this.id || (typeof this.id == 'string' && this.id.substr(0, 3) == 'new'));

                var $menuBtn = this.$container.find('> .actions > .settings'),
                    menuBtn = new Garnish.MenuBtn($menuBtn);

                this.$actionMenu = menuBtn.menu.$container;

                menuBtn.menu.settings.onOptionSelect = $.proxy(this, 'onMenuOptionSelect');

                // Collapse by default for old qlinks, expand if new qlinks
                if (! this.isNew)
                {
                    this.collapse();
                }

                this.addListener(this.$titlebar, 'dblclick', function(ev)
                {
                    ev.preventDefault();
                    this.toggle();
                });
            },

            toggle: function()
            {
                if (this.collapsed)
                {
                    this.expand();
                }
                else
                {
                    this.collapse(true);
                }
            },

            collapse: function(animate)
            {
                if (this.collapsed)
                {
                    return;
                }

                this.$container.addClass('collapsed');

                var previewHtml = '',
                    $fields = this.$fieldsContainer.children();

                for (var i = 0; i < $fields.length; i++)
                {
                    var $field = $($fields[i]),
                        $inputs = $field.children('.input').find('select,input[type!="hidden"],textarea,.label'),
                        inputPreviewText = '';

                    for (var j = 0; j < $inputs.length; j++)
                    {
                        var $input = $($inputs[j]);

                        if ($input.hasClass('label'))
                        {
                            var $maybeLightswitchContainer = $input.parent().parent();

                            if ($maybeLightswitchContainer.hasClass('lightswitch') && (
                                    ($maybeLightswitchContainer.hasClass('on') && $input.hasClass('off')) ||
                                    (!$maybeLightswitchContainer.hasClass('on') && $input.hasClass('on'))
                                ))
                            {
                                continue;
                            }

                            var value = $input.text();
                        }
                        else
                        {
                            var value = Craft.getText(Garnish.getInputPostVal($input));
                        }

                        if (value instanceof Array)
                        {
                            value = value.join(', ');
                        }

                        if (value)
                        {
                            value = Craft.trim(value);

                            if (value)
                            {
                                if (inputPreviewText)
                                {
                                    inputPreviewText += ', ';
                                }

                                inputPreviewText += value;
                            }
                        }
                    }

                    if (inputPreviewText)
                    {
                        previewHtml += (previewHtml ? ' <span>|</span> ' : '') + inputPreviewText;
                    }
                }

                this.$previewContainer.html(previewHtml);

                this.$fieldsContainer.velocity('stop');
                this.$container.velocity('stop');

                if (animate)
                {
                    this.$fieldsContainer.velocity('fadeOut', { duration: 'fast' });
                    this.$container.velocity({ height: 16 }, 'fast');
                }
                else
                {
                    this.$previewContainer.show();
                    this.$fieldsContainer.hide();
                    this.$container.css({ height: 16 });
                }

                setTimeout($.proxy(function() {
                    this.$actionMenu.find('a[data-action=collapse]:first').parent().addClass('hidden');
                    this.$actionMenu.find('a[data-action=expand]:first').parent().removeClass('hidden');
                }, this), 200);

                // Remember that?
                if (!this.isNew)
                {
                    Craft.CompanyQuickLinks.rememberCollapsedBlockId(this.id);
                }
                else
                {
                    if (!this.$collapsedInput)
                    {
                        this.$collapsedInput = $('<input type="hidden" name="'+this.matrix.inputNamePrefix+'['+this.id+'][collapsed]" value="1"/>').appendTo(this.$container);
                    }
                    else
                    {
                        this.$collapsedInput.val('1');
                    }
                }

                this.collapsed = true;
            },

            expand: function()
            {
                if (!this.collapsed)
                {
                    return;
                }

                this.$container.removeClass('collapsed');

                this.$fieldsContainer.velocity('stop');
                this.$container.velocity('stop');

                var collapsedContainerHeight = this.$container.height();
                this.$container.height('auto');
                this.$fieldsContainer.show();
                var expandedContainerHeight = this.$container.height();
                this.$container.height(collapsedContainerHeight);
                this.$fieldsContainer.hide().velocity('fadeIn', { duration: 'fast' });
                this.$container.velocity({ height: expandedContainerHeight }, 'fast', $.proxy(function() {
                    this.$previewContainer.html('');
                    this.$container.height('auto');
                }, this));

                setTimeout($.proxy(function() {
                    this.$actionMenu.find('a[data-action=collapse]:first').parent().removeClass('hidden');
                    this.$actionMenu.find('a[data-action=expand]:first').parent().addClass('hidden');
                }, this), 200);

                // Remember that?
                if (!this.isNew && typeof Storage !== 'undefined')
                {
                    var collapsedBlocks = Craft.CompanyQuickLinks.getCollapsedBlockIds(),
                        collapsedBlocksIndex = $.inArray(''+this.id, collapsedBlocks);

                    if (collapsedBlocksIndex != -1)
                    {
                        collapsedBlocks.splice(collapsedBlocksIndex, 1);
                        Craft.CompanyQuickLinks.setCollapsedBlockIds(collapsedBlocks);
                    }
                }

                if (!this.isNew)
                {
                    Craft.CompanyQuickLinks.forgetCollapsedBlockId(this.id);
                }
                else if (this.$collapsedInput)
                {
                    this.$collapsedInput.val('');
                }

                this.collapsed = false;
            },

            disable: function()
            {
                this.$container.children('input[name$="[enabled]"]:first').val('');
                this.$container.addClass('disabled');

                setTimeout($.proxy(function() {
                    this.$actionMenu.find('a[data-action=disable]:first').parent().addClass('hidden');
                    this.$actionMenu.find('a[data-action=enable]:first').parent().removeClass('hidden');
                }, this), 200);

                this.collapse(true);
            },

            enable: function()
            {
                this.$container.children('input[name$="[enabled]"]:first').val('1');
                this.$container.removeClass('disabled');

                setTimeout($.proxy(function() {
                    this.$actionMenu.find('a[data-action=disable]:first').parent().removeClass('hidden');
                    this.$actionMenu.find('a[data-action=enable]:first').parent().addClass('hidden');
                }, this), 200);
            },

            onMenuOptionSelect: function(option)
            {
                var batchAction = (this.matrix.blockSelect.totalSelected > 1 && this.matrix.blockSelect.isSelected(this.$container)),
                    $option = $(option);

                switch ($option.data('action'))
                {
                    case 'collapse':
                    {
                        if (batchAction)
                        {
                            this.matrix.collapseSelectedBlocks();
                        }
                        else
                        {
                            this.collapse(true);
                        }

                        break;
                    }

                    case 'expand':
                    {
                        if (batchAction)
                        {
                            this.matrix.expandSelectedBlocks();
                        }
                        else
                        {
                            this.expand();
                        }

                        break;
                    }

                    case 'add':
                    {
                        var type = $option.data('type');
                        this.matrix.addBlock(type, this.$container);
                        break;
                    }

                    case 'clone':
                    {
                        var qLinkNum = $option.data('qlinks-num');
                        this.matrix.cloneQLinks(qLinkNum);
                        break;
                    }

                    case 'delete':
                    {
                        if (confirm(Craft.t('Are you sure you want to delete the quick links?')))
                        {
                            this.selfDestruct();
                        }

                        break;
                    }
                }
            },

            selfDestruct: function()
            {
                this.$container.velocity(this.matrix.getHiddenBlockCss(this.$container), 'fast', $.proxy(function()
                {
                    this.$container.remove();
                    this.matrix.updateAddBlockBtn();
                }, this));
            }
        });

})(jQuery);