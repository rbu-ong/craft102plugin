 $(function () {
    $('main[role="main"]').addClass('single-pane');

    $($('#settings-tabs  a.settings')).on('click', function (e) {
        e.preventDefault();
        var menu_active = $('#' + this.id);
        $('.settings').not(menu_active).removeClass('selected');
        $(menu_active).css({'outline': 'none'}).addClass('selected');
        var id = this.href.split('#')[1];
        var active = $('#' + id);
        if (active.length) {
            $('.settings-tab-content').not(active).hide();
            active.css('display', 'block');
        }
    });

    if($('input[name="companyId"]').length > 0) {
        $('#tabs a.main').css('display', 'block');
        $('#display_user-field').hide();
    }

    $('#businessType').on('click', function () {
        if($(this).attr('aria-checked') == 'true') {
            $('#tabs ul li').css('display', 'block');
            $('#display_user-field').hide();
        } else {
            $('#tabs li a').not('.tab.sel').parent().hide();
            $('#display_user-field').show();
        }
    });

    $('#tabs a.tab').on('click', function (e) {
        e.preventDefault();
        var menu_active = $('#' + this.id);
        $('.tab').not(menu_active).removeClass('sel');
        $(menu_active).addClass('sel');
        var id = this.href.split('#')[1];
        var active = $('#' + id);
        if (active.length) {
            $('.tab-content').not(active).hide();
            active.css('display', 'block');
        }
    });
});