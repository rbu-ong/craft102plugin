<?php

namespace craft\accompanies\controllers;

use accompanies\Accompanies;

use Craft;
use craft\web\Controller;
use craft\accompanies\Plugin;
use craft\elements\Asset;
use craft\accompanies\elements\Plugin as elementPlugin;

class DefaultController extends Controller
{
    // public $allowAnonymous = [
    //     'actionCommunities',
    //     'actionPublicEntries',
    //     'actionJobs'
    // ];

    // protected $_siteTemplate;

    // /**
    //  * Initialize controller.
    //  */
    // function __construct()
    // {
    //     $settings = $this->plugin->settings;
    //     $this->_siteTemplate = preg_replace('/{theme}/', craft()->config->get('theme'), $settings->siteTemplate);
    // }

    public function actionIndex()
    {
        $this->renderTemplate('accompanies');
    }

    public function actionSettings()
    {
        $this->renderTemplate('accompanies/settings');
    }

    public function actionCommunities(array $variables = [])
    {
        $criteria = ['order' => 'title ASC'];
        $interests = craft()->aCCompanies->getTopicsCount();
        $companies = craft()->aCCompanies->getPremiumCompanies($criteria);
        $tag = '';

        if (isset($variables['tag']))
        {
            $tag = craft()->aCCompanies->getTags($variables['tag']);
            $companies = craft()->aCCompanies->getElementsByTopic($criteria, $tag);
        }

        $this->renderTemplate($this->_siteTemplate, compact('companies', 'interests', 'tag'));
    }

    public function actionJobs(array $variables = [])
    {
        $company = craft()->aCCompanies->getCompanyByUsername($variables['username']);
        $properties = craft()->aCCompanies->prepProperties($company);
        $jobs = craft()->aCJobs->getAll(['slug' => $variables['job']])->first();

        $this->renderTemplate($this->_siteTemplate . '/_entry', compact('company', 'properties', 'jobs'));
    }

    public function actionCareers(array $variables = [])
    {
        $company = craft()->aCCompanies->getCompanyByUsername($variables['username']);
        $properties = craft()->aCCompanies->prepProperties($company);
        $variables['regions'] = $this->getRegions($company->id);

        $variables['jobs'] = $this->getJobs($variables, $company->id);
        $variables['tags'] = craft()->aCJobs->getBrowseTags($variables['jobs']->find());

        $variables['tagTerm'] = '';

        if (isset($variables['browse']))
        {
            if ($variables['browse'] == 'tag') $variables['tagTerm'] = $variables['term'];
            elseif (isset($variables['tag'])) $variables['tagTerm'] = $variables['tag'];
        }

        $this->renderTemplate($this->_siteTemplate . '/_entry', compact('company', 'properties', 'variables'));
    }

    /**
     * Delete a company.
     *
     * @throws Exception
     */
    public function actionDelete()
    {
        $this->requirePostRequest();

        $companyId = craft()->request->getRequiredPost('companyId');
        $company = craft()->aCCompanies->find($companyId);

        if (!$company) {
            throw new Exception(Craft::t('No company exists with the ID “{id}”.', array('id' => $companyId)));
        }

        if (craft()->aCCompanies->delete($company)) {
            if (craft()->request->isAjaxRequest()) {
                $this->returnJson(array('success' => true));
            } else {
                craft()->userSession->setNotice(Craft::t('Company deleted.'));
                $this->redirectToPostedUrl($company);
            }
        } else {
            if (craft()->request->isAjaxRequest()) {
                $this->returnJson(array('success' => false));
            } else {
                craft()->userSession->setError(Craft::t('Couldn’t delete the company.'));

                // Send the entry back to the template
                craft()->urlManager->setRouteVariables(array(
                    'company' => $company
                ));
            }
        }
    }

    /**
     * Display company data.
     *
     * @param array $variables
     * @return mixed
     * @throws HttpException
     */
    public function actionEdit(int $companyId = null)
    {
        // $settings = services::$plugin->companyServices->getSettings();

        $variables = [
            'companyId' => $companyId
        ];

        if (empty($variables['company'])) {
            if (!empty($variables['companyId'])) {
                $variables['company'] = Plugin::$plugin->companyServices->find( $variables['companyId'] );
                if (!$variables['company'])
                {
                    throw new HttpException(404);
                }
            } 
            else {
                // $variables['company'] = new ACCompaniesModel();
                $variables['company'] = Plugin::$plugin->companyModel;
            }
        }

        $variables['title'] = $variables['company']->id ? $variables['company']->title : 'Create a new company';
        // $variables['company'] = [ 'username' => '' ];
        // $variables['tagGroupId'] =  $settings->tagGroupId;

        // // Set asset logo
        // $variables['assetId'] = $variables['company']->assetId;
        $variables['assetId'] = '';
        // $variables['elements'] = $variables['assetId'] ? [craft()->elements->getElementById($variables['assetId'])] : [];

        $variables['elements'] = [];

        // // Set asset banner
        $variables['bannerId'] = '';
        $variables['bannerElements'] = [];
        // $variables['bannerElements'] = $variables['bannerId'] ? [craft()->elements->getElementById($variables['bannerId'])] : [];

        // if ($variables['company']->properties['quick_links'])
        // {
        //     $variables['qLinks'] = craft()->aCCompanies->prepLinks($variables['company']->properties['quick_links']);
        // }

        // $variables['empty_slot'] = [
        //     'id' => 'slot',
        //     'name' => 'slot',
        //     'required' => 'true',
        //     'cols' => [
        //         'linktext' => [
        //             'heading' => 'Link Text',
        //             'type' => 'singleline'
        //         ],
        //         'linkurl' => [
        //             'heading' => 'link URL',
        //             'type' => 'singleline'
        //         ]
        //     ],
        //     'rows' => ''
        // ];

        // // Can't just use the entry's getCpEditUrl() because that might include the locale ID when we don't want it
        // $variables['baseCpEditUrl'] = 'eewebextras/accompanies/{id}';

        // // Set the "Continue Editing" URL
        // $variables['continueEditingUrl'] = $variables['baseCpEditUrl'] .
        //     (craft()->isLocalized() && craft()->getLanguage() != $variables['localeId'] ? '/' . $variables['localeId'] : '');

        // print_r( $variables );
        // die;

        return $this->renderTemplate('accompanies/edit', $variables);
    }

 
    /**
     * Save model.
     * @return mixed
     * @throws Exception
     */
    public function actionSave(array $variables = [])
    {
        $this->requirePostRequest();
        $request = Craft::$app->getRequest();

        // if ($companyId = $request->getBodyParam('companyId'))
        // {
        //     $company = Plugin::$plugin->companyServices->find($companyId);
        //     if (!$company)
        //     {
        //         throw new Exception(Craft::t('No company exists with ID --> "{id}"', [
        //             'id' => $company
        //         ]));
        //     }
        // }
        // else
        // {
        //     $company = new Plugin::$plugin->companyModel();
        // }

        // $topics = $request->getBodyParam('topics', []);
        // $settings = $request->getBodyParam('settings', []);
        // $accountSettings = $this->_prepAccountSettings($request->getBodyParam('accountSettings', []));
        // // $qLinks = Craft::$app->aCCompanies->jsonifyLinks(Craft::$app->request->getBodyParam());

        // try
        // {
        //     $company->id = $request->getBodyParam('companyId', $company->id);
        //     $company->title = $request->getBodyParam('title', $company->title);
        //     $company->description = $request->getBodyParam('description', $company->description);
        //     $company->email = $request->getBodyParam('email', $company->email);
        //     $company->username = $request->getBodyParam('username', $company->username);
        //     // $company->enabled = (bool)Craft::$app->request->getBodyParam('enabled', $company->enabled);
        //     // $company->slug = Craft::$app->request->getBodyParam('username', $company->username);
        //     $company->assetId = (!empty($request->getBodyParam('assetId', $company->assetId)) ? $request->getBodyParam('assetId', $company->assetId)[0] : null);
        //     $company->bannerId = (!empty($request->getBodyParam('bannerId', $company->bannerId)) ? $request->getBodyParam('bannerId', $company->bannerId)[0] : null);
        //     // $company->properties = ['topics' => $topics, 'settings' => $settings, 'quick_links' => $qLinks];
        //     $company->accountSettings = $accountSettings;

        //     if (!Plugin::$plugin->companyServices->isValidUsername($company->username))
        //     {
        //         $company->addError('username', Craft::t('Invalid username (only accepts small letters, numbers and  -).'));

        //         Craft::$app->urlManager->setRouteVariables([
        //             'company' => $company
        //         ]);
        //     }
        //     else
        //     {
        //         // // create user
        //         // if (!$company->userId) {
        //         //     $user = services::$plugin->companyServices->createUser($company);

        //         //     if ($user instanceof UserModel) {
        //         //         $company->userId = $user->id;
        //         //     } else {
        //         //         // Error bag?
        //         //         if (is_array($user)) {
        //         //             $company->addErrors($user);
        //         //         }

        //         //         throw new \Exception(Craft::t('Unable to save company entry.'));
        //         //     }
        //         // }
        //     }

        //     // save
        //     if (Plugin::$plugin->companyServices->save($company))
        //     {
        //         // Craft::$app->userSession->setNotice(Craft::t('Company entry saved.'));
        //         // $this->redirectToPostedUrl($company);
        //     }
        //     else
        //     {
        //         throw new \Exception(Craft::t('Unable to save company entry.'));
        //     }
        // }
        // catch(\Exception $e)
        // {
        //     echo $e->getMessage();
        //     die;

        //     Craft::$app->getSession()->setError(Craft::t('app', $e->getMessage()));
        //     // Craft::$app->urlManager->setRouteVariables([
        //     //     'company' => $company
        //     // ]);
        // }

        $company = new elementPlugin();

        $company->title = $request->getBodyParam('title', $company->title);

        Craft::$app->getElements()->saveElement($company);
    }

    private function _prepAccountSettings($settings)
    {
        foreach ($settings as &$setting)
        {
            if ($setting == '')
            {
                $setting = '0';
            }
        }

        return $settings;
    }
}