<?php
namespace craft\accompanies;

use craft\events\RegisterUrlRulesEvent;
use craft\web\UrlManager;
use yii\base\Event;

class Plugin extends \craft\base\Plugin
{
	// Static Properties
    // =========================================================================

    /**
     * Static property that is an instance of this plugin class so that it can be accessed via
     * Guide::$plugin
     *
     * @var Guide
     */
    public static $plugin;

    // Public Methods
    // =========================================================================

    /**
     * Set our $plugin static property to this class so that it can be accessed via
     * Guide::$plugin
     *
     * Called after the plugin class is instantiated; do any one-time initialization
     * here such as hooks and events.
     *
     * If you have a '/vendor/autoload.php' file, it will be loaded for you automatically;
     * you do not need to load it in your init() method.
     *
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $this->setComponents([
            'companyServices' => \craft\accompanies\services\CompanyServices::class,
            'companyModel' => \craft\accompanies\models\CompanyModel::class,
            'userModel' => \craft\accompanies\models\CompanyModel::class,
        ]);

        // Custom initialization code goes here...
		Event::on(
			UrlManager::class
			,UrlManager::EVENT_REGISTER_CP_URL_RULES
			,function(RegisterUrlRulesEvent $event) {
		    	$event->rules['accompanies'] = ['template' => 'accompanies/index', 'variables' => ['title' => 'Guide']];
                $event->rules['accompanies/settings'] = 'accompanies/default/settings';
                // $event->rules['accompanies/settings'] = ['template' => 'accompanies/settings', 'variables' => ['title' => 'Settings']];
                $event->rules['accompanies/new'] = 'accompanies/default/edit';
                $event->rules['accompanies/<companyId:\d+>'] = 'accompanies/default/edit';
			}
		);

    }
}