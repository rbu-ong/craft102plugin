<?php
/**
 * @link      https://craftcms.com/
 * @copyright Copyright (c) Pixel & Tonic, Inc.
 * @license   https://craftcms.github.io/license/
 */

namespace craft\accompanies\records;

use craft\db\ActiveRecord;
use yii\db\ActiveQueryInterface;

class AccompaniesRecord extends ActiveRecord
{
    /**
     * Gets the database table name
     *
     * @return string
     */
     public static function tableName(): string
    {
        return '{{%accompanies}}';
    }

    /**
     * Returns the category’s element.
     *
     * @return ActiveQueryInterface The relational query object.
     */
    public function getElement(): ActiveQueryInterface
    {
        return $this->hasOne(Element::class, ['id' => 'id']);
    }
}
