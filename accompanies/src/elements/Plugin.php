<?php
namespace craft\accompanies\elements;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQueryInterface;
use craft\accompanies\elements\db\CompanyQuery;
use craft\elements\actions\Delete;
use craft\accompanies\Plugin as mainPlugin;
use craft\helpers\UrlHelper;

class Plugin extends Element
{
    public $price = 0;
    public $userId;
    public $description;
    public $accountSettings;
    public $hasCareer;
    public $username;
    public $email;
    // ...

    public static function hasTitles(): bool
    {
        return true;
    }

    public static function hasContent(): bool
    {
        return true;
    }

    // public function getEditorHtml(): string
    // {
    //     $html = \Craft::$app->getView()->renderTemplateMacro('_includes/forms', 'textField', [
    //         [
    //             'label' => \Craft::t('app', 'Title'),
    //             'siteId' => $this->siteId,
    //             'id' => 'title',
    //             'name' => 'title',
    //             'value' => $this->title,
    //             'errors' => $this->getErrors('title'),
    //             'first' => true,
    //             'autofocus' => true,
    //             'required' => true
    //         ]
    //     ]);

    //     // ...

    //     $html .= parent::getEditorHtml();

    //     return $html;
    // }

    // public function getFieldLayout()
    // {
    //     return \Craft::$app->fields->getLayoutByType(Product::class);
    // }

    public static function isLocalized(): bool
    {
        return true;
    }

    /**
     * @inheritdoc
     */
    protected static function defineActions(string $source = null): array
    {
        // Now figure out what we can do with these
        $actions = [];

        $actions[] = Craft::$app->getElements()->createAction([
            'type' => Delete::class,
            'confirmationMessage' => Craft::t('app', 'Are you sure you want to delete the selected entries?'),
            'successMessage' => Craft::t('app', 'Products deleted.'),
        ]);

        return $actions;
    }

    public function afterSave(bool $isNew)
    {
    //     if ($isNew) {
    //         \Craft::$app->db->createCommand()
    //             ->insert('{{%products}}', [
    //                 'id' => $this->id,
    //                 'price' => $this->price,
    //                 'currency' => $this->currency,
    //             ])
    //             ->execute();
    //     } else {
    //         \Craft::$app->db->createCommand()
    //             ->update('{{%products}}', [
    //                 'price' => $this->price,
    //                 'currency' => $this->currency,
    //             ], ['id' => $this->id])
    //             ->execute();
    //     }
        $request = Craft::$app->getRequest();

        if ($companyId = $request->getBodyParam('companyId'))
        {
            $company = mainPlugin::$plugin->companyServices->find($companyId);
            if (!$company)
            {
                throw new Exception(Craft::t('No company exists with ID --> "{id}"', [
                    'id' => $company
                ]));
            }
        }
        else
        {
            $company = new mainPlugin::$plugin->companyModel();
        }

        try
        {
            $accountSettings = $this->_prepAccountSettings($request->getBodyParam('accountSettings', []));

            // $company->id = $request->getBodyParam('companyId', $company->id);
            $company->title = $request->getBodyParam('title', $company->title);
            $company->description = $request->getBodyParam('description', $company->description);
            $company->email = $request->getBodyParam('email', $company->email);
            $company->username = $request->getBodyParam('username', $company->username);
            $company->assetId = (!empty($request->getBodyParam('assetId', $company->assetId)) ? $request->getBodyParam('assetId', $company->assetId)[0] : null);
            $company->bannerId = (!empty($request->getBodyParam('bannerId', $company->bannerId)) ? $request->getBodyParam('bannerId', $company->bannerId)[0] : null);
            $company->accountSettings = $accountSettings;
            $company->id = $this->id;

            if (mainPlugin::$plugin->companyServices->save($company))
            {
                // Craft::$app->userSession->setNotice(Craft::t('Company entry saved.'));
                // $this->redirectToPostedUrl($company);
            }
            else
            {
                throw new \Exception(Craft::t('Unable to save company entry.'));
            }
        }
        catch(\Exception $e)
        {
            echo $e->getMessage();
            die;

            Craft::$app->getSession()->setError(Craft::t('app', $e->getMessage()));
            // Craft::$app->urlManager->setRouteVariables([
            //     'company' => $company
            // ]);
        }
        parent::afterSave($isNew);
    }

    public static function find(): ElementQueryInterface
    {
        return new CompanyQuery(static::class);
    }

    protected static function defineSources(string $context = null): array
    {
        return [
            [
                'key' => '*',
                'label' => 'All Products',
                'criteria' => []
            ],
        ];
    }

    // public function getCpEditUrl()
    // {
    //     return 'products/products/'.$this->id;
    // }

    // protected static function defineActions(string $source = null): array
    // {
    //     return [
    //         FooAction::class,
    //         BarAction::class,
    //     ];
    // }

    private function _prepAccountSettings($settings)
    {
        foreach ($settings as &$setting)
        {
            if ($setting == '')
            {
                $setting = '0';
            }
        }

        return $settings;
    }

    protected static function defineSortOptions(): array
    {
        return [
            'title' => \Craft::t('products', 'Title'),
            'accountSettings' => \Craft::t('products', 'Business Type'),
        ];
    }

    protected static function defineTableAttributes(): array
    {
        return [
            'title' => \Craft::t('products', 'Title'),
            'accountSettings' => \Craft::t('products', 'Business Type'),
            'hasCareer' => \Craft::t('products', 'Careers / Jobs'),
            // 'accountSettings' => \Craft::t('products', 'Careers / Jobs'),
            // 'accountSettings' => \Craft::t('products', 'Display User Profile?'),
        ];
    }

    protected static function defineDefaultTableAttributes(string $source): array
    {
        return ['title', 'accountSettings'];
    }

    protected function tableAttributeHtml(string $attribute): string
    {
        switch ($attribute) {
            case 'accountSettings':
                $accountSettings = json_decode( $this->accountSettings );
                if ($accountSettings->businessType == '1')
                {
                    $businessType = 'Premium';
                }
                else
                {
                    $businessType = 'Free';
                }
                return $businessType;

            case 'hasCareer':
                $accountSettings = json_decode( $this->accountSettings );

                if ($accountSettings->hasCareer == '1')
                {
                    $hasCareer = 'Show';
                }
                else
                {
                    $hasCareer = '';
                }
                return $hasCareer;

            default:
                return parent::tableAttributeHtml($attribute);
        //     case 'price':
        //         return \Craft::$app->formatter->asCurrency($this->price, $this->currency);

        //     case 'currency':
        //         return strtoupper($this->currency);
        }

        // return parent::tableAttributeHtml($attribute);
    }

    public function getCpEditUrl()
    {
        $url = UrlHelper::cpUrl('accompanies/'.$this->id);

        return $url;
    }

}