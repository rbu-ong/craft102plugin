<?php
namespace craft\accompanies\elements\db;

use craft\db\Query;
use craft\elements\db\ElementQuery;
use craft\helpers\Db;
use craft\accompanies\elements\Company;

class CompanyQuery extends ElementQuery
{
    public $accountSettings;

    public function accountSettings($value)
    {
        $this->accountSettings = $value;

        return $this;
    }

    protected function beforePrepare(): bool
    {
        // join in the products table
        $this->joinElementTable('accompanies');

        // select the price column
        $this->query->select([
            'accompanies.userId',
            'accompanies.description',
            'accompanies.accountSettings',
            // 'accompanies.properties',
            'accompanies.email',
            'accompanies.username',
        ]);

        // if ($this->price) {
        //     $this->subQuery->andWhere(Db::parseParam('products.price', $this->price));
        // }

        // if ($this->currency) {
        //     $this->subQuery->andWhere(Db::parseParam('products.currency', $this->currency));
        // }

        return parent::beforePrepare();
    }
}