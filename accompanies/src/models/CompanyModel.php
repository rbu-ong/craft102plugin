<?php
namespace craft\accompanies\models;

use craft\accompanies\Plugin;

use Craft;
use craft\base\Model;

class CompanyModel extends Model
{
    public $id;
    public $title;
    public $properties;
    public $description;
    public $accountSettings;
    public $assetId;
    public $bannerId;
    public $userId;
    public $email;
    public $username;

    /**
     * Set model attributes
     *
     * @return array
     */
    public function rules()
    {
        return [
            [['id'],'integer','required'],
            [['title', 'properties', 'description', 'accountSettings'], 'mixed'],
            [['assetId', 'bannerId','userId'], 'integer'],
            [['email'], 'email'],
            [['username'], 'string']
        ];
    }
}
