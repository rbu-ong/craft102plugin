<?php
namespace craft\tagmanager\elements;

use Craft;
use craft\base\Element;
use craft\elements\db\ElementQueryInterface;
use craft\elements\actions\Delete;

class Plugin extends Element
{

    public static function hasTitles(): bool
	{
	    return true;
	}

	public static function hasContent(): bool
	{
	    return true;
	}

	public static function isLocalized(): bool
	{
	    return true;
	}

	/**
     * @inheritdoc
     */
    protected static function defineActions(string $source = null): array
    {
        // Now figure out what we can do with these
        $actions = [];

        $actions[] = Craft::$app->getElements()->createAction([
            'type' => Delete::class,
            'confirmationMessage' => Craft::t('app', 'Are you sure you want to delete the selected entries?'),
            'successMessage' => Craft::t('app', 'Products deleted.'),
        ]);

        return $actions;
    }

 //    public function afterSave(bool $isNew)
	// {
	//     if ($isNew) {
	//         \Craft::$app->db->createCommand()
	//             ->insert('{{%products}}', [
	//                 'id' => $this->id,
	//                 'price' => $this->price,
	//                 'currency' => $this->currency,
	//             ])
	//             ->execute();
	//     } else {
	//         \Craft::$app->db->createCommand()
	//             ->update('{{%products}}', [
	//                 'price' => $this->price,
	//                 'currency' => $this->currency,
	//             ], ['id' => $this->id])
	//             ->execute();
	//     }

	//     parent::afterSave($isNew);
	// }

    protected static function defineSources(string $context = null): array
	{
	    return [
	        [
	            'key' => '*',
	            'label' => 'All Products',
	            'criteria' => []
	        ],
	        // [
	        //     'key' => 'cad',
	        //     'label' => 'CAD',
	        //     'criteria' => [
	        //         'currency' => 'cad',
	        //     ]
	        // ],
	        // [
	        //     'key' => 'usd',
	        //     'label' => 'USD',
	        //     'criteria' => [
	        //         'currency' => 'usd',
	        //     ]
	        // ],
	    ];
	}

	// public function getCpEditUrl()
	// {
	//     return 'products/products/'.$this->id;
	// }

	// protected static function defineActions(string $source = null): array
	// {
	//     return [
	//         FooAction::class,
	//         BarAction::class,
	//     ];
	// }

	protected static function defineSortOptions(): array
	{
	    return [
	        'title' => \Craft::t('app', 'Title'),
	    ];
	}

	protected static function defineTableAttributes(): array
	{
	    return [
	        'title' => \Craft::t('app', 'Title'),
	    ];
	}

	protected static function defineDefaultTableAttributes(string $source): array
	{
	    return ['title'];
	}

	protected function tableAttributeHtml(string $attribute): string
	{
	    return parent::tableAttributeHtml($attribute);
	}

}