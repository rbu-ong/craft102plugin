<?php

namespace craft\acjobs\migrations;

use Craft;
use craft\db\Migration;

/**
 * m180109_044245_pluginmigrationfile migration.
 */
class Install extends Migration
{
    /**
     * @inheritdoc
     */
    public function safeUp()
    {
        // Place migration code here...
        if (!$this->db->tableExists('{{%acjobs}}')) {
            // create the products table
            $this->createTable('{{%acjobs}}', [
                'id' => $this->integer()->notNull(),
                'jobId' => $this->integer(),
                // 'jobLevel' => $this->integer(),
                'workRemotely' => $this->integer(),
                'description' => $this->text(),
                'accountSettings' => $this->text(),
                'applicationUrl' => $this->text(),
                'postDate' => $this->dateTime(),
                'expiryDate' => $this->dateTime(),
                'dateCreated' => $this->dateTime()->notNull(),
                'dateUpdated' => $this->dateTime()->notNull(),
                'uid' => $this->uid(),
                'PRIMARY KEY(id)',
            ]);

            // give it a FK to the elements table
            $this->addForeignKey(
                $this->db->getForeignKeyName('{{%acjobs}}', 'id'),
                '{{%acjobs}}', 'id', '{{%elements}}', 'id', 'CASCADE', null);
        }
    }

    /**
     * @inheritdoc
     */
    public function safeDown()
    {
        // if ($this->db->tableExists('{{%accompanies}}')) {
        //     $this->dropTable('{{%accompanies}}');
        // }
    }
}
