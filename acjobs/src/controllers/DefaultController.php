<?php

namespace craft\acjobs\controllers;

use acjobs\Acjobs;

use Craft;
use craft\web\Controller;


class DefaultController extends Controller
{
    // public $allowAnonymous = [
    //     'actionCommunities',
    //     'actionPublicEntries',
    //     'actionJobs'
    // ];

    // protected $_siteTemplate;

    // /**
    //  * Initialize controller.
    //  */
    // function __construct()
    // {
    //     $settings = $this->plugin->settings;
    //     $this->_siteTemplate = preg_replace('/{theme}/', craft()->config->get('theme'), $settings->siteTemplate);
    // }

    public function actionIndex()
    {
        $this->renderTemplate('acjobs');
    }

    public function actionSettings()
    {
        $this->renderTemplate('acjobs/settings');
    }

    public function actionCommunities(array $variables = [])
    {
        $criteria = ['order' => 'title ASC'];
        $interests = craft()->aCCompanies->getTopicsCount();
        $companies = craft()->aCCompanies->getPremiumCompanies($criteria);
        $tag = '';

        if (isset($variables['tag']))
        {
            $tag = craft()->aCCompanies->getTags($variables['tag']);
            $companies = craft()->aCCompanies->getElementsByTopic($criteria, $tag);
        }

        $this->renderTemplate($this->_siteTemplate, compact('companies', 'interests', 'tag'));
    }

    public function actionJobs(array $variables = [])
    {
        $company = craft()->aCCompanies->getCompanyByUsername($variables['username']);
        $properties = craft()->aCCompanies->prepProperties($company);
        $jobs = craft()->aCJobs->getAll(['slug' => $variables['job']])->first();

        $this->renderTemplate($this->_siteTemplate . '/_entry', compact('company', 'properties', 'jobs'));
    }

    public function actionCareers(array $variables = [])
    {
        $company = craft()->aCCompanies->getCompanyByUsername($variables['username']);
        $properties = craft()->aCCompanies->prepProperties($company);
        $variables['regions'] = $this->getRegions($company->id);

        $variables['jobs'] = $this->getJobs($variables, $company->id);
        $variables['tags'] = craft()->aCJobs->getBrowseTags($variables['jobs']->find());

        $variables['tagTerm'] = '';

        if (isset($variables['browse']))
        {
            if ($variables['browse'] == 'tag') $variables['tagTerm'] = $variables['term'];
            elseif (isset($variables['tag'])) $variables['tagTerm'] = $variables['tag'];
        }

        $this->renderTemplate($this->_siteTemplate . '/_entry', compact('company', 'properties', 'variables'));
    }

    /**
     * Delete a company.
     *
     * @throws Exception
     */
    public function actionDelete()
    {
        $this->requirePostRequest();

        $companyId = craft()->request->getRequiredPost('companyId');
        $company = craft()->aCCompanies->find($companyId);

        if (!$company) {
            throw new Exception(Craft::t('No company exists with the ID “{id}”.', array('id' => $companyId)));
        }

        if (craft()->aCCompanies->delete($company)) {
            if (craft()->request->isAjaxRequest()) {
                $this->returnJson(array('success' => true));
            } else {
                craft()->userSession->setNotice(Craft::t('Company deleted.'));
                $this->redirectToPostedUrl($company);
            }
        } else {
            if (craft()->request->isAjaxRequest()) {
                $this->returnJson(array('success' => false));
            } else {
                craft()->userSession->setError(Craft::t('Couldn’t delete the company.'));

                // Send the entry back to the template
                craft()->urlManager->setRouteVariables(array(
                    'company' => $company
                ));
            }
        }
    }

    /**
     * Display company data.
     *
     * @param array $variables
     * @return mixed
     * @throws HttpException
     */
    public function actionEdit(array $variables = [])
    {
        $settings = $this->getPlugin()->getSettings();

        if (empty($variables['company'])) {
            if (!empty($variables['accompanyId'])) {
                $variables['company'] = craft()->aCCompanies->find($variables['accompanyId']);
                if (!$variables['company'])
                {
                    throw new HttpException(404);
                }
            } else {
                $variables['company'] = new ACCompaniesModel();
            }
        }

        $variables['title'] = $variables['company']->id ? $variables['company']->title : Craft::t('Create a new company');
        $variables['tagGroupId'] =  $settings->tagGroupId;

        // Set asset logo
        $variables['assetId'] = $variables['company']->assetId;
        $variables['elements'] = $variables['assetId'] ? [craft()->elements->getElementById($variables['assetId'])] : [];

        // Set asset banner
        $variables['bannerId'] = $variables['company']->bannerId;
        $variables['bannerElements'] = $variables['bannerId'] ? [craft()->elements->getElementById($variables['bannerId'])] : [];

        if ($variables['company']->properties['quick_links'])
        {
            $variables['qLinks'] = craft()->aCCompanies->prepLinks($variables['company']->properties['quick_links']);
        }

        $variables['empty_slot'] = [
            'id' => 'slot',
            'name' => 'slot',
            'required' => 'true',
            'cols' => [
                'linktext' => [
                    'heading' => 'Link Text',
                    'type' => 'singleline'
                ],
                'linkurl' => [
                    'heading' => 'link URL',
                    'type' => 'singleline'
                ]
            ],
            'rows' => ''
        ];

        // Can't just use the entry's getCpEditUrl() because that might include the locale ID when we don't want it
        $variables['baseCpEditUrl'] = 'eewebextras/accompanies/{id}';

        // Set the "Continue Editing" URL
        $variables['continueEditingUrl'] = $variables['baseCpEditUrl'] .
            (craft()->isLocalized() && craft()->getLanguage() != $variables['localeId'] ? '/' . $variables['localeId'] : '');

        return $this->renderTemplate('accompanies/edit', $variables);
    }


    /**
     * Save model.
     * @return mixed
     * @throws Exception
     */
    public function actionSave()
    {
        $this->requirePostRequest();

        if ($companyId = craft()->request->getPost('companyId'))
        {
            $company = craft()->aCCompanies->find($companyId);
            if (!$company)
            {
                throw new Exception(Craft::t('No company exists with ID --> "{id}"', [
                    'id' => $company
                ]));
            }
        }
        else
        {
            $company = new ACCompaniesModel();
        }

        $topics = craft()->request->getPost('topics', []);
        $settings = craft()->request->getPost('settings', []);
        $accountSettings = $this->_prepAccountSettings(craft()->request->getPost('accountSettings', []));
        $qLinks = craft()->aCCompanies->jsonifyLinks(craft()->request->getPost());

        try
        {
            $company->id = craft()->request->getPost('companyId', $company->id);
            $company->getContent()->title = craft()->request->getPost('title', $company->title);
            $company->description = craft()->request->getPost('description', $company->description);
            $company->email = craft()->request->getPost('email', $company->email);
            $company->username = craft()->request->getPost('username', $company->username);
            $company->enabled = (bool)craft()->request->getPost('enabled', $company->enabled);
            $company->slug = craft()->request->getPost('username', $company->username);
            $company->assetId = (!empty(craft()->request->getPost('assetId', $company->assetId)) ? craft()->request->getPost('assetId', $company->assetId)[0] : null);
            $company->bannerId = (!empty(craft()->request->getPost('bannerId', $company->bannerId)) ? craft()->request->getPost('bannerId', $company->bannerId)[0] : null);
            $company->properties = ['topics' => $topics, 'settings' => $settings, 'quick_links' => $qLinks];
            $company->accountSettings = $accountSettings;

            if (!craft()->aCCompanies->isValidUsername($company->username))
            {
                $company->addError('username', Craft::t('Invalid username (only accepts small letters, numbers and  -).'));

                craft()->urlManager->setRouteVariables([
                    'company' => $company
                ]);
            }
            else
            {
                // create user
                if (!$company->userId) {
                    $user = craft()->aCCompanies->createUser((object)craft()->request->getPost());

                    if ($user instanceof UserModel) {
                        $company->userId = $user->id;
                    } else {
                        // Error bag?
                        if (is_array($user)) {
                            $company->addErrors($user);
                        }

                        throw new \Exception(Craft::t('Unable to save company entry.'));
                    }
                }
            }

            // save
            if (craft()->aCCompanies->save($company))
            {
                craft()->userSession->setNotice(Craft::t('Company entry saved.'));
                $this->redirectToPostedUrl($company);
            }
            else
            {
                throw new \Exception(Craft::t('Unable to save company entry.'));
            }
        }
        catch(\Exception $e)
        {
            craft()->userSession->setError($e->getMessage());

            craft()->urlManager->setRouteVariables([
                'company' => $company
            ]);
        }
    }

    private function _prepAccountSettings($settings)
    {
        foreach ($settings as &$setting)
        {
            if ($setting == '')
            {
                $setting = '0';
            }
        }

        return $settings;
    }
}