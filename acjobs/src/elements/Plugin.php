<?php
namespace craft\acjobs\elements;

use Craft;
use craft\base\Element;
use craft\controllers\ElementIndexesController;
use craft\db\Query;
use craft\elements\actions\Delete;
use craft\elements\actions\Edit;
use craft\elements\actions\NewChild;
use craft\elements\actions\SetStatus;
use craft\elements\actions\View;
use craft\elements\db\ElementQuery;
use craft\elements\db\ElementQueryInterface;
use craft\elements\db\EntryQuery;

class Plugin extends Element
{
    /**
     * @var int Price
     */
    public $price = 0;

    /**
     * @var string Currency code
     */
    public $currency;
    
    /**
     * @inheritdoc
     */
    public static function displayName(): string
    {
        return Craft::t('app', 'Jobs');
    }

	public function afterSave(bool $isNew)
	{
	    if ($isNew) {
	        \Craft::$app->db->createCommand()
	            ->insert('{{%products}}', [
	                'id' => $this->id,
	                'price' => $this->price,
	                'currency' => $this->currency,
	            ])
	            ->execute();
	    } else {
	        \Craft::$app->db->createCommand()
	            ->update('{{%products}}', [
	                'price' => $this->price,
	                'currency' => $this->currency,
	            ], ['id' => $this->id])
	            ->execute();
	    }

	    parent::afterSave($isNew);
	}

	/**
     * @inheritdoc
     */
    public static function hasContent(): bool
    {
        return true;
    }

	/**
     * @inheritdoc
     */
    public static function hasTitles(): bool
    {
        return true;
    }

	/**
     * @inheritdoc
     */
    public static function isLocalized(): bool
    {
        return true;
    }

	protected static function defineTableAttributes(): array
	{
	    return [
	        'title' => \Craft::t('app', 'Title'),
	        'description' => \Craft::t('app', 'Description'),
	    ];
	}

	protected static function defineSources(string $context = null): array
	{
	    return [
	        [
	            'key' => '*',
	            'label' => 'Companies',
	            'criteria' => []
	        ]
	    ];
	}

	protected static function prepElementQueryForTableAttribute(ElementQueryInterface $elementQuery, string $attribute)
    {
        /** @var ElementQuery $elementQuery */
        if ($attribute === 'author') {
            $with = $elementQuery->with ?: [];
            $with[] = 'author';
            $elementQuery->with = $with;
        } else {
            parent::prepElementQueryForTableAttribute($elementQuery, $attribute);
        }
    }

	protected static function defineDefaultTableAttributes(string $source): array
	{
	    return [
	    	'title'
	    	,'workRemotely'
	    ];
	}

	protected function tableAttributeHtml(string $attribute): string
	{
	    switch ($attribute) {
	        case 'price':
	            return \Craft::$app->formatter->asCurrency($this->price, $this->currency);

	        case 'currency':
	            return strtoupper($this->currency);

            case 'description':
                return $this->description;
	    }

	    return parent::tableAttributeHtml($attribute);
	}

	/**
     * @inheritdoc
     */
    public function afterMoveInStructure(int $structureId)
    {
        // Was the entry moved within its section's structure?
        $section = $this->getSection();

        if ($section->type == Section::TYPE_STRUCTURE && $section->structureId == $structureId) {
            Craft::$app->getElements()->updateElementSlugAndUri($this, true, true, true);
        }

        parent::afterMoveInStructure($structureId);
    }

    // Private Methods
    // =========================================================================

    /**
     * Returns whether the entry has been assigned a new parent entry.
     *
     * @return bool
     * @see beforeSave()
     * @see afterSave()
     */
    private function _hasNewParent(): bool
    {
        if ($this->_hasNewParent !== null) {
            return $this->_hasNewParent;
        }

        return $this->_hasNewParent = $this->_checkForNewParent();
    }

    /**
     * Checks if the entry has been assigned a new parent entry.
     *
     * @return bool
     * @see _hasNewParent()
     */
    private function _checkForNewParent(): bool
    {
        // Make sure this is a Structure section
        if ($this->getSection()->type != Section::TYPE_STRUCTURE) {
            return false;
        }

        // Is it a brand new entry?
        if ($this->id === null) {
            return true;
        }

        // Was a new parent ID actually submitted?
        if ($this->newParentId === null) {
            return false;
        }

        // Is it set to the top level now, but it hadn't been before?
        if (!$this->newParentId && $this->level != 1) {
            return true;
        }

        // Is it set to be under a parent now, but didn't have one before?
        if ($this->newParentId && $this->level == 1) {
            return true;
        }

        // Is the parentId set to a different entry ID than its previous parent?
        $oldParentQuery = self::find();
        $oldParentQuery->ancestorOf($this);
        $oldParentQuery->ancestorDist(1);
        $oldParentQuery->status(null);
        $oldParentQuery->siteId($this->siteId);
        $oldParentQuery->enabledForSite(false);
        $oldParentQuery->select('elements.id');
        $oldParentId = $oldParentQuery->scalar();

        return $this->newParentId != $oldParentId;
    }

}