<?php
namespace ns\acjobs\elements\db;

use craft\db\Query;
use craft\elements\db\ElementQuery;
use craft\helpers\Db;
use ns\prefix\elements\Product;

class AcjobsQuery extends ElementQuery
{
    public $price;
    public $currency;

    public function price($value)
    {
        $this->price = $value;

        return $this;
    }

    public function currency($value)
    {
        $this->currency = $value;

        return $this;
    }

    protected function beforePrepare(): bool
    {
        // join in the products table
        $this->joinElementTable('acjobs');

        // select the price column
        $this->query->select([
            'acjobs.workRemotely',
            'acjobs.description',
        ]);

        // if ($this->price) {
        //     $this->subQuery->andWhere(Db::parseParam('products.price', $this->price));
        // }

        // if ($this->currency) {
        //     $this->subQuery->andWhere(Db::parseParam('products.currency', $this->currency));
        // }

        return parent::beforePrepare();
    }
}